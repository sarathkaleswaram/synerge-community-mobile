# Synerge Community 


### Notification icon
link - https://github.com/arnesson/cordova-plugin-firebase/issues/764
```xml
<config-file parent="/manifest/application/" target="app/src/main/AndroidManifest.xml">
    <meta-data android:name="com.google.firebase.messaging.default_notification_icon" android:resource="@drawable/notification_icon" />
</config-file>
```

## Deploy Android
Link - https://ionicframework.com/docs/deployment/play-store

### New release
```bat
ionic cordova build android --prod --release

keytool -genkey -v -keystore my-release-key.keystore -alias alias_name -keyalg RSA -keysize 2048 -validity 10000

jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore my-release-key.keystore .\notification-test\platforms\android\app\build\outputs\apk\release\app-release-unsigned.apk alias_name

C:\Users\acer\AppData\Local\Android\Sdk\build-tools\28.0.3\zipalign.exe -v 4 .\notification-test\platforms\android\app\build\outputs\apk\release\app-release-unsigned.apk synerge_community_signed_v1.0.5.apk
```
### Update release
```bat
> Change version number.
> Check API URL.

ionic cordova build android --prod --release

jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore .\deploy\android\my-release-key.keystore .\platforms\android\app\build\outputs\apk\release\app-release-unsigned.apk alias_name

C:\Users\acer\AppData\Local\Android\Sdk\build-tools\28.0.3\zipalign.exe -v 4 .\platforms\android\app\build\outputs\apk\release\app-release-unsigned.apk synerge_community_signed_v1.0.5.apk
```

### Generate Privacy Policy
Link - https://app-privacy-policy-generator.firebaseapp.com/