import { Component, OnInit } from '@angular/core';
import { AlertController, IonRouterOutlet, ModalController, PopoverController } from '@ionic/angular';
import { AchieverDetailsComponent } from 'src/app/components/achiever-details/achiever-details.component';
import { AchieverComponent } from 'src/app/components/achiever/achiever.component';
import { ActionPopoverPage } from 'src/app/components/action-popover/action-popover';
import { Achiever } from 'src/app/domains/models/achievers';
import { ResponseFormat } from 'src/app/domains/response-format';
import { ApiService } from 'src/app/services/api.service';
import { LoggerService } from 'src/app/services/logger.service';
import { SocketService } from 'src/app/services/socket.service';
import { SynergeService } from 'src/app/services/synerge.service';

@Component({
  selector: 'app-achievers',
  templateUrl: './achievers.page.html',
  styleUrls: ['./achievers.page.scss'],
})
export class AchieversPage implements OnInit {
  synergeIAchiever: Achiever[];
  synergeIIAchiever: Achiever[];
  displayAchievers: Achiever[];
  selectedSegment = 'SYNERGE_I';

  constructor(
    public synergeService: SynergeService,
    private socketService: SocketService,
    private apiService: ApiService,
    private modalController: ModalController,
    public popoverCtrl: PopoverController,
    public routerOutlet: IonRouterOutlet,
    private alertController: AlertController,
    private logger: LoggerService
  ) { }

  ngOnInit() {
    this.getAllAchiever();
  }

  doRefresh(event) {
    this.getAllAchiever(event);
  }

  segmentChanged(ev: any) {
    this.selectedSegment = ev.detail.value;
    this.changeDisplayEvents();
  }

  changeDisplayEvents() {
    if (this.selectedSegment === 'SYNERGE_I')
      this.displayAchievers = [...this.synergeIAchiever];
    else
      this.displayAchievers = [...this.synergeIIAchiever];
  }

  getAllAchiever(event?) {
    this.apiService.getAllAchievers().subscribe((res: ResponseFormat) => {
      this.logger.debug("Got Total Achievers count: " + res.data.length);
      if (event) event.target.complete();
      if (res.success) {
        let totalAchievers: Achiever[] = res.data;
        this.synergeIAchiever = totalAchievers.filter(x => x.belongs_to === "SYNERGE_I");
        this.logger.debug("Synerge I Achievers count: " + this.synergeIAchiever.length);
        this.synergeIIAchiever = totalAchievers.filter(x => x.belongs_to === "SYNERGE_II");
        this.logger.debug("Synerge II Achievers count: " + this.synergeIIAchiever.length);
        this.changeDisplayEvents();
      } else {
        this.synergeService.handleError(res.message);
      }
    }, error => {
      if (event) event.target.complete();
      this.synergeService.handleErrorOnAPI(error);
    });
  }

  async onDetails(id) {
    const modal = await this.modalController.create({
      component: AchieverDetailsComponent,
      mode: 'ios',
      swipeToClose: true,
      presentingElement: this.routerOutlet.nativeEl,
      componentProps: { achieverId: id }
    });
    await modal.present();

    const { data } = await modal.onWillDismiss();
    if (data) {
      this.getAllAchiever();
    }
  }

  async presentPopover(event, achiever: Achiever) {
    const popover = await this.popoverCtrl.create({
      component: ActionPopoverPage,
      event,
      componentProps: {
        showEdit: true,
        showDelete: true
      }
    });
    await popover.present();

    const { data } = await popover.onWillDismiss();
    this.logger.debug("Popover data: " + JSON.stringify(data));
    if (data) {
      if (data.action === 'Edit') {
        this.editAchiever(achiever);
      }
      if (data.action === 'Delete') {
        this.deleteAchiever(achiever);
      }
    }
  }

  async editAchiever(achiever: Achiever) {
    const modal = await this.modalController.create({
      component: AchieverComponent,
      componentProps: { achieverId: achiever._id }
    });
    await modal.present();

    const { data } = await modal.onWillDismiss();
    if (data) {
      this.getAllAchiever();
    }
  }

  async deleteAchiever(achiever: Achiever) {
    this.logger.debug("On Delete: " + JSON.stringify(achiever));
    const alert = await this.alertController.create({
      header: 'Confirm!',
      message: `Do you want to delete ${achiever.achiever}?`,
      buttons: [
        {
          text: 'No',
          role: 'cancel',
        }, {
          text: 'Yes',
          handler: () => {
            this.apiService.deleteAchieverById(achiever._id).subscribe((res: ResponseFormat) => {
              this.logger.debug("Achiever Deleted by ID: " + JSON.stringify(res));
              if (res.success) {
                setTimeout(() => {
                  this.getAllAchiever();
                }, 1000);
              } else {
                this.synergeService.handleError(res.message);
              }
            }, error => {
              this.synergeService.handleErrorOnAPI(error);
            });
          }
        }
      ]
    });
    await alert.present();
  }

  async onAdd() {
    const modal = await this.modalController.create({
      component: AchieverComponent,
    });
    await modal.present();

    const { data } = await modal.onWillDismiss();
    if (data) {
      this.getAllAchiever();
    }
  }


}
