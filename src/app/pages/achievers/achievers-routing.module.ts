import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AchieversPage } from './achievers.page';

const routes: Routes = [
  {
    path: '',
    component: AchieversPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AchieversPageRoutingModule {}
