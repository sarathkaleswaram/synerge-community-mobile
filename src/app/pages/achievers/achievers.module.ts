import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AchieversPageRoutingModule } from './achievers-routing.module';

import { AchieversPage } from './achievers.page';
import { ComponentsModule } from 'src/app/components/components.module';
// import { Crop } from '@ionic-native/crop/ngx';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AchieversPageRoutingModule,
    ComponentsModule
  ],
  declarations: [AchieversPage],
  // providers: [Crop]
})
export class AchieversPageModule {}
