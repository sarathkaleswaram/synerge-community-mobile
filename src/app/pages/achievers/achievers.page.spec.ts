import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AchieversPage } from './achievers.page';

describe('AchieversPage', () => {
  let component: AchieversPage;
  let fixture: ComponentFixture<AchieversPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AchieversPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AchieversPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
