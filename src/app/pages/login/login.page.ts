import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { AppConstants } from 'src/app/domains/app-constants';
import { ResponseFormat } from 'src/app/domains/response-format';
import { ApiService } from 'src/app/services/api.service';
import { LoggerService } from 'src/app/services/logger.service';
import * as md5 from 'md5';
import { SynergeService } from 'src/app/services/synerge.service';
import { AlertController, LoadingController, MenuController } from '@ionic/angular';
import { SocketService } from 'src/app/services/socket.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  body = {
    user_name: "",
    password: ""
  }
  buttonText = "Let`s go";
  submitted = false;

  constructor(
    public alertController: AlertController,
    public loadingController: LoadingController,
    private apiService: ApiService,
    private logger: LoggerService,
    private synergeService: SynergeService,
    private socketService: SocketService,
    private menu: MenuController,
    public router: Router
  ) { }

  ngOnInit() {
    this.menu.enable(false);
  }

  onLogin(form: NgForm) {
    this.submitted = true;

    if (form.valid) {
      this.buttonText = "Loading...";
      this.apiService.loginUser({ user_name: this.body.user_name, password: md5(this.body.password), mobile: true }).subscribe((res: ResponseFormat) => {
        if (res.success) {
          this.buttonText = "Success";
          this.logger.debug("Login routes: " + JSON.stringify(res.data.routes));
          localStorage.setItem(AppConstants.ROUTES, JSON.stringify(res.data.routes));
          localStorage.setItem(AppConstants.TOKEN, res.token);
          localStorage.setItem(AppConstants.USER_FULL_NAME, res.data.user.first_name + " " + res.data.user.last_name);
          localStorage.setItem(AppConstants.USER_ID, res.data.user._id);
          localStorage.setItem(AppConstants.USER_ROLE, res.data.user.role);
          localStorage.setItem(AppConstants.USER_BELONGS_TO, res.data.user.belongs_to);
          localStorage.setItem(AppConstants.USER_COMPANY_NAME, res.data.user.company_name);
          this.router.navigate(['/tabs/news-feed'], { replaceUrl: true });
          this.menu.enable(true);
          this.synergeService.setMenuItems();
          this.socketService.connectSocket();
          this.synergeService.isAdmin = res.data.user.role === 'ADMIN' ? true : false;
          // setTimeout(() => {
          //   this.notificationService.openBetaVersionSnackBar();
          // }, 1000);
        } else {
          this.buttonText = "Let`s go";
          this.synergeService.displayAlert(res.message);
        }
      }, error => {
        this.buttonText = "Let`s go";
        this.synergeService.displayAlert(error.message);
      });
    }
  }

  onRegister() {
    this.router.navigateByUrl('/register');
  }

  async onForgotPassword() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Forgot Password?',
      inputs: [
        {
          name: 'email',
          id: 'email',
          type: 'email',
          placeholder: 'Enter Email'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary'
        }, {
          text: 'Ok',
          handler: (data) => {
            if (data.email) {
              this.sendForgotPassword(data.email);
            } else {
              this.synergeService.displayAlert('Enter Email address');
            }
          }
        }
      ]
    });

    await alert.present();
  }

  async sendForgotPassword(email: string) {
    const loading = await this.loadingController.create({
      message: 'Please wait...',
      duration: 5000
    });
    await loading.present();
    this.apiService.forgotPassword({ email: email }).subscribe((res: ResponseFormat) => {
      loading.dismiss();
      if (res.success) {
        this.synergeService.displayToast(res.message);
      } else {
        this.synergeService.displayAlert(res.message);
      }
    }, error => {
      loading.dismiss();
      this.synergeService.displayAlert(error.message);
    });
  }

}
