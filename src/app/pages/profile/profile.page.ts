import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ModalController, PopoverController } from '@ionic/angular';
import { ActionPopoverPage } from 'src/app/components/action-popover/action-popover';
import { EditProfileComponent } from 'src/app/components/edit-profile/edit-profile.component';
import { AppConstants } from 'src/app/domains/app-constants';
import { User } from 'src/app/domains/models/user';
import { ResponseFormat } from 'src/app/domains/response-format';
import { ApiService } from 'src/app/services/api.service';
import { LoggerService } from 'src/app/services/logger.service';
import { SynergeService } from 'src/app/services/synerge.service';

@Component({
  selector: 'app-profile',
  templateUrl: 'profile.page.html',
  styleUrls: ['profile.page.scss']
})
export class ProfilePage implements OnInit {
  user: User;

  constructor(private apiService: ApiService,
    public synergeService: SynergeService,
    private modalController: ModalController,
    private router: Router,
    public popoverCtrl: PopoverController,
    private logger: LoggerService) { }

  ngOnInit() {
    this.getUser();
  }

  getUser() {
    this.apiService.getUserById(localStorage.getItem(AppConstants.USER_ID)).subscribe(data => {
      this.logger.debug("My Profile - User by ID: " + JSON.stringify(data));
      this.user = data.data;
    }, error => {
      this.synergeService.handleErrorOnAPI(error);
    });
  }

  onEditClick(id) {
    this.router.navigate(["/edit-profile"], { queryParams: { user_id: id } });
  }

  async presentPopover(event: Event) {
    const popover = await this.popoverCtrl.create({
      component: ActionPopoverPage,
      event,
      componentProps: {
        showEdit: true,
        showChangePhoto: true,
        showRemovePhoto: true
      }
    });
    await popover.present();

    const { data } = await popover.onWillDismiss();
    this.logger.debug("Popover data: " + JSON.stringify(data));
    if (data) {
      if (data.action === 'Edit') {
        this.editProfile();
      }
      if (data.action === 'ChangePhoto') {
        this.changePhoto(data.data);
      }
      if (data.action === 'RemovePhoto') {
        this.removePhoto();
      }
    }
  }

  async editProfile() {
    const modal = await this.modalController.create({
      component: EditProfileComponent,
      componentProps: { id: localStorage.getItem(AppConstants.USER_ID) }
    });
    await modal.present();

    const { data } = await modal.onWillDismiss();
    if (data) {
      this.getUser();
    }
  }

  changePhoto({ croppedImage, croppedFile }) {
    if (croppedFile) {
      let formData = new FormData();
      if (this.synergeService.isMobile) {
        let file = this.synergeService.dataURItoBlob(croppedImage);
        formData.append('uploadImage', file, `carousel-${(new Date().getTime()).toString(16)}.png`);
      } else {
        formData.append('uploadImage', croppedFile);
      }
      this.apiService.uploadImage(formData).subscribe((res: ResponseFormat) => {
        if (res.success) {
          this.user.profile_pic_path = '/uploads/' + res.data;
          this.apiService.updateUser(this.user).subscribe((res: ResponseFormat) => {
            this.logger.debug("Update Profile - User by ID: " + JSON.stringify(res));
            if (res.success) {
              this.getUser();
            } else {
              this.synergeService.handleError(res.message);
            }
          }, error => {
            this.synergeService.handleErrorOnAPI(error);
          });
        }
      }, error => {
        this.synergeService.handleErrorOnAPI(error);
      });
    }
  }

  async removePhoto() {
    this.user.profile_pic_path = null;
    this.apiService.updateUser(this.user).subscribe((res: ResponseFormat) => {
      this.logger.debug("Update Profile - User by ID: " + JSON.stringify(res));
      if (res.success) {
        this.getUser();
      } else {
        this.synergeService.handleError(res.message);
      }
    }, error => {
      this.synergeService.handleErrorOnAPI(error);
    });
  }

}
