import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AlertController, ModalController } from '@ionic/angular';
import { AddBookingComponent } from 'src/app/components/add-booking/add-booking.component';
import { BookingDetailsComponent } from 'src/app/components/booking-details/booking-details.component';
import { SelectClientCompanyComponent } from 'src/app/components/select-client-company/select-client-company.component';
import { AppConstants } from 'src/app/domains/app-constants';
import { Booking } from 'src/app/domains/models/bookings';
import { ResponseFormat, SocketEventFormat } from 'src/app/domains/response-format';
import { ApiService } from 'src/app/services/api.service';
import { LoggerService } from 'src/app/services/logger.service';
import { SocketService } from 'src/app/services/socket.service';
import { SynergeService } from 'src/app/services/synerge.service';

@Component({
  selector: 'app-bookings',
  templateUrl: 'bookings.page.html',
  styleUrls: ['bookings.page.scss']
})
export class BookingsPage implements OnInit {
  bookings: Booking[];

  constructor(
    public synergeService: SynergeService,
    private socketService: SocketService,
    private apiService: ApiService,
    private route: ActivatedRoute,
    private logger: LoggerService,
    public alertController: AlertController,
    private modalController: ModalController
  ) { }

  ngOnInit() {
    this.socketService.resetDotNotification();
    this.route.queryParams.subscribe(params => {
      let bookingId = params['bookingId'];
      if (bookingId) {
        this.apiService.getBookingById(bookingId).subscribe((res: ResponseFormat) => {
          this.logger.debug("Got Booking by id: " + JSON.stringify(res));
          if (res.data) {
            this.onBookingSelect(res.data);
          } else {
            this.synergeService.handleError(res.message);
          }
        }, error => {
          this.synergeService.handleErrorOnAPI(error);
        });
      }
    });
    this.getBookings();
    this.socketService.socketObservable.subscribe((data: SocketEventFormat) => {
      if (data.type === AppConstants.TYPE_BOOKING) {
        this.logger.debug("Socket Bookings component update");
        this.getBookings();
      }
    });
  }

  doRefresh(event) {
    this.getBookings(event);
  }

  getBookings(event?) {
    this.apiService.getAllBookings().subscribe((res: ResponseFormat) => {
      this.logger.debug("Got Bookings count: " + res.data.length);
      if (event) event.target.complete();
      if (res.success) {
        this.bookings = res.data;
      } else {
        this.synergeService.handleError(res.message);
      }
    }, error => {
      if (event) event.target.complete();
      this.synergeService.handleErrorOnAPI(error);
    });
  }

  async onDetails(id) {
    if (!this.synergeService.isAdmin) return;
    const modal = await this.modalController.create({
      component: BookingDetailsComponent,
      componentProps: { id: id }
    });
    await modal.present();

    const { data } = await modal.onWillDismiss();
    if (data) {
      this.getBookings();
    }
  }

  onConfirm(id: String) {
    let booking = this.bookings.find(b => b._id === id);
    if (!booking) return;
    this.logger.debug("Confirming booking: " + JSON.stringify(booking));
    booking.status = "Confirmed";
    this.apiService.updateBookingById(booking).subscribe((res: ResponseFormat) => {
      this.logger.debug("Update Booking: " + JSON.stringify(res));
      if (res.success) {
        this.getBookings();
      } else {
        this.synergeService.handleError(res.message);
      }
    }, error => {
      this.synergeService.handleErrorOnAPI(error);
    });
  }

  onDecline(id: String) {
    let booking = this.bookings.find(b => b._id === id);
    if (!booking) return;
    this.logger.debug("Declineing booking: " + JSON.stringify(booking));
    booking.status = "Declined";
    this.apiService.updateBookingById(booking).subscribe((res: ResponseFormat) => {
      this.logger.debug("Update Booking: " + JSON.stringify(res));
      if (res.success) {
        this.getBookings();
      } else {
        this.synergeService.handleError(res.message);
      }
    }, error => {
      this.synergeService.handleErrorOnAPI(error);
    });
  }

  async onDelete(id: String) {
    let booking = this.bookings.find(b => b._id === id);
    if (!booking) return;
    const alert = await this.alertController.create({
      header: 'Confirm!',
      message: 'Do you want to delete booking?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
        }, {
          text: 'Yes',
          handler: () => {
            this.logger.debug("Deleting booking: " + JSON.stringify(booking));
            this.apiService.deleteBookingById(booking._id).subscribe((res: ResponseFormat) => {
              this.logger.debug("Delete Booking: " + JSON.stringify(res));
              this.getBookings();
              if (!res.success) {
                this.synergeService.handleError(res.message);
              }
            }, error => {
              this.synergeService.handleErrorOnAPI(error);
            });
          }
        }
      ]
    });
    await alert.present();
  }

  onBookingSelect(booking: Booking) {
    // const dialogRef = this.dialog.open(EditBookingDialogComponent, { width: '800px', data: booking });

    // dialogRef.afterClosed().subscribe(result => {
    //   this.getBookings();
    // });
  }

  async onAdd() {
    if (this.synergeService.isAdmin) {
      const modal = await this.modalController.create({
        component: SelectClientCompanyComponent
      });
      await modal.present();

      const { data } = await modal.onWillDismiss();
      if (data) {
        this.addBooking(data);
      }
    } else {
      let data = {
        belongs_to: localStorage.getItem(AppConstants.USER_BELONGS_TO),
        company_name: localStorage.getItem(AppConstants.USER_COMPANY_NAME)
      }
      this.addBooking(data);
    }
  }

  async addBooking(bookingData) {
    const modal = await this.modalController.create({
      component: AddBookingComponent,
      componentProps: {
        belongs_to: bookingData.belongs_to,
        company_name: bookingData.company_name
      }
    });
    await modal.present();

    const { data } = await modal.onWillDismiss();
    if (data) {
      this.getBookings();
    }
  }

}
