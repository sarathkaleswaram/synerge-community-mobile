import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { BookingsPage } from './bookings.page';
import { ExploreContainerComponentModule } from '../explore-container/explore-container.module';

import { BookingsPageRoutingModule } from './bookings-routing.module';
import { ComponentsModule } from 'src/app/components/components.module';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    ExploreContainerComponentModule,
    BookingsPageRoutingModule,
    ComponentsModule
  ],
  declarations: [BookingsPage]
})
export class BookingsPageModule {}
