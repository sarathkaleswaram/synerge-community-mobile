import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SynergeFamilyPage } from './synerge-family.page';

const routes: Routes = [
  {
    path: '',
    component: SynergeFamilyPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SynergeFamilyPageRoutingModule {}
