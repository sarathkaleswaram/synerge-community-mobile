import { Component, OnInit } from '@angular/core';
import { AlertController, ModalController, PopoverController } from '@ionic/angular';
import { ActionPopoverPage } from 'src/app/components/action-popover/action-popover';
import { SynergeFamilyComponent } from 'src/app/components/synerge-family/synerge-family.component';
import { SynergeFamily } from 'src/app/domains/models/synergeFamily';
import { ResponseFormat } from 'src/app/domains/response-format';
import { ApiService } from 'src/app/services/api.service';
import { LoggerService } from 'src/app/services/logger.service';
import { SynergeService } from 'src/app/services/synerge.service';

@Component({
  selector: 'app-synerge-family',
  templateUrl: './synerge-family.page.html',
  styleUrls: ['./synerge-family.page.scss'],
})
export class SynergeFamilyPage implements OnInit {
  synergeI: SynergeFamily[];
  synergeII: SynergeFamily[];
  displaySynergeFamily: SynergeFamily[];
  selectedSegment = 'SYNERGE_I';

  constructor(
    public synergeService: SynergeService,
    private apiService: ApiService,
    private modalController: ModalController,
    public popoverCtrl: PopoverController,
    private alertController: AlertController,
    private logger: LoggerService
  ) { }

  ngOnInit() {
    this.getAllSynergeFamily();
  }

  doRefresh(event) {
    this.getAllSynergeFamily(event);
  }

  segmentChanged(ev: any) {
    this.selectedSegment = ev.detail.value;
    this.changeDisplayEvents();
  }

  changeDisplayEvents() {
    if (this.selectedSegment === 'SYNERGE_I')
      this.displaySynergeFamily = [...this.synergeI];
    else
      this.displaySynergeFamily = [...this.synergeII];
  }

  getAllSynergeFamily(event?) {
    this.apiService.getAllSynergeFamilys().subscribe((res: ResponseFormat) => {
      this.logger.debug("Got Total Synerge Familys count: " + res.data.length);
      if (event) event.target.complete();
      if (res.success) {
        let totalSynergeFamily: SynergeFamily[] = res.data;
        this.synergeI = totalSynergeFamily.filter(x => x.belongs_to === "SYNERGE_I");
        this.logger.debug("SynergeI count: " + this.synergeI.length);
        this.synergeII = totalSynergeFamily.filter(x => x.belongs_to === "SYNERGE_II");
        this.logger.debug("SynergeII count: " + this.synergeII.length);
        this.changeDisplayEvents();
      } else {
        this.synergeService.handleError(res.message);
      }
    }, error => {
      if (event) event.target.complete();
      this.synergeService.handleErrorOnAPI(error);
    });
  }

  async presentPopover(event, synerge: SynergeFamily) {
    const popover = await this.popoverCtrl.create({
      component: ActionPopoverPage,
      event,
      componentProps: {
        showEdit: true,
        showDelete: true
      }
    });
    await popover.present();

    const { data } = await popover.onWillDismiss();
    this.logger.debug("Popover data: " + JSON.stringify(data));
    if (data) {
      if (data.action === 'Edit') {
        this.editSynergeFamily(synerge);
      }
      if (data.action === 'Delete') {
        this.deleteSynergeFamily(synerge);
      }
    }
  }

  async editSynergeFamily(synergeFamily: SynergeFamily) {
    const modal = await this.modalController.create({
      component: SynergeFamilyComponent,
      componentProps: { synergeFamilyId: synergeFamily._id }
    });
    await modal.present();

    const { data } = await modal.onWillDismiss();
    if (data) {
      this.getAllSynergeFamily();
    }
  }

  async deleteSynergeFamily(synergeFamily: SynergeFamily) {
    this.logger.debug("On Delete: " + JSON.stringify(synergeFamily));
    const alert = await this.alertController.create({
      header: 'Confirm!',
      message: `Do you want to delete ${synergeFamily.company_name}?`,
      buttons: [
        {
          text: 'No',
          role: 'cancel',
        }, {
          text: 'Yes',
          handler: () => {
            this.apiService.deleteSynergeFamilyById(synergeFamily._id).subscribe((res: ResponseFormat) => {
              this.logger.debug("SynergeFamily Deleted by ID: " + JSON.stringify(res));
              if (res.success) {
                setTimeout(() => {
                  this.getAllSynergeFamily();
                }, 1000);
              } else {
                this.synergeService.handleError(res.message);
              }
            }, error => {
              this.synergeService.handleErrorOnAPI(error);
            });
          }
        }
      ]
    });
    await alert.present();
  }

  async onAdd() {
    const modal = await this.modalController.create({
      component: SynergeFamilyComponent,
    });
    await modal.present();

    const { data } = await modal.onWillDismiss();
    if (data) {
      this.getAllSynergeFamily();
    }
  }
}
