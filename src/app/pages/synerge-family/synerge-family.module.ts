import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SynergeFamilyPageRoutingModule } from './synerge-family-routing.module';

import { SynergeFamilyPage } from './synerge-family.page';
import { ComponentsModule } from 'src/app/components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SynergeFamilyPageRoutingModule,
    ComponentsModule
  ],
  declarations: [SynergeFamilyPage]
})
export class SynergeFamilyPageModule { }
