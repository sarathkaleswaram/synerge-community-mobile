import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SynergeFamilyPage } from './synerge-family.page';

describe('SynergeFamilyPage', () => {
  let component: SynergeFamilyPage;
  let fixture: ComponentFixture<SynergeFamilyPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SynergeFamilyPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SynergeFamilyPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
