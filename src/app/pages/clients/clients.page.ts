import { Component, OnInit } from '@angular/core';
import { AlertController, ModalController } from '@ionic/angular';
import { ClientComponent } from 'src/app/components/client/client.component';
import { Clients } from 'src/app/domains/models/clients';
import { ResponseFormat } from 'src/app/domains/response-format';
import { ApiService } from 'src/app/services/api.service';
import { LoggerService } from 'src/app/services/logger.service';
import { SynergeService } from 'src/app/services/synerge.service';

@Component({
  selector: 'app-clients',
  templateUrl: './clients.page.html',
  styleUrls: ['./clients.page.scss'],
})
export class ClientsPage implements OnInit {
  clients = [];
  deleteButtonText: string[] = [];

  constructor(
    private apiService: ApiService,
    public synergeService: SynergeService,
    private alertController: AlertController,
    private modalController: ModalController,
    private logger: LoggerService
  ) { }

  ngOnInit() {
    this.getClientsList();
  }

  doRefresh(event) {
    this.getClientsList(event);
  }

  getClientsList(event?) {
    this.apiService.getAllClients().subscribe((res: ResponseFormat) => {
      this.logger.debug("Got clients count: " + res.data.length);
      if (event) event.target.complete();
      this.clients = res.data;
      this.clients.forEach((client: Clients, i: number) => {
        this.deleteButtonText[i] = 'Delete';
      });
    }, error => {
      if (event) event.target.complete();
      this.synergeService.handleErrorOnAPI(error);
    });
  }

  async onAdd() {
    const modal = await this.modalController.create({
      component: ClientComponent
    });
    await modal.present();

    const { data } = await modal.onWillDismiss();
    if (data) {
      this.getClientsList();
    }
  }

  async onEdit(client: Clients) {
    const modal = await this.modalController.create({
      component: ClientComponent,
      componentProps: { clientId: client._id }
    });
    await modal.present();

    const { data } = await modal.onWillDismiss();
    if (data) {
      this.getClientsList();
    }
  }

  async onDelete(client: Clients, i: number) {
    this.logger.debug("On Delete: " + JSON.stringify(client));
    const alert = await this.alertController.create({
      header: 'Confirm!',
      message: `Do you want to delete ${client.company_name}?`,
      buttons: [
        {
          text: 'No',
          role: 'cancel',
        }, {
          text: 'Yes',
          handler: () => {
            this.deleteButtonText[i] = "Loading...";
            this.apiService.deleteClientById(client).subscribe((res: ResponseFormat) => {
              this.logger.debug("Client Deleted by ID: " + JSON.stringify(res));
              if (res.success) {
                setTimeout(() => {
                  this.getClientsList();
                }, 1000);
              } else {
                this.synergeService.handleError(res.message);
              }
            }, error => {
              this.synergeService.handleErrorOnAPI(error);
            });
          }
        }
      ]
    });
    await alert.present();
  }

}
