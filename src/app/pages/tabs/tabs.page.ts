import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Platform } from '@ionic/angular';
import { FCM } from "cordova-plugin-fcm-with-dependecy-updated/ionic/ngx";
import { AppConstants } from 'src/app/domains/app-constants';
import { ResponseFormat, SocketEventFormat } from 'src/app/domains/response-format';
import { ApiService } from 'src/app/services/api.service';
import { LoggerService } from 'src/app/services/logger.service';
import { SynergeService } from 'src/app/services/synerge.service';
import { SocketService } from 'src/app/services/socket.service';

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage implements OnInit {

  constructor(
    private fcm: FCM,
    public platform: Platform,
    private apiService: ApiService,
    public synergeService: SynergeService,
    private socketService: SocketService,
    public router: Router,
    private logger: LoggerService
  ) { }

  ngOnInit() {
    this.platform.ready().then(() => {
      if (this.synergeService.isMobile) {
        this.subscribeToTopic();
        this.getToken();
        // subscribe 
        this.fcm.onNotification().subscribe(data => {
          let path = (data && data.path) ? decodeURIComponent(data.path) : undefined;
          this.logger.debug("Path: " + path);
          if (data.wasTapped) {
            console.log("Received in background", data);
            if (path)
              this.router.navigateByUrl(path);
          } else {
            console.log("Received in foreground", data);
            if (path) {
              let buttons = [
                {
                  side: 'end',
                  // icon: 'star',
                  text: 'Open',
                  handler: () => {
                    this.router.navigateByUrl(path);
                  }
                }
              ]
              this.synergeService.displayToast(data.body, data.title, 'top', buttons, 3000);
            }
            else
              this.synergeService.displayToast(data.body, data.title, 'top', undefined, 3000);
          };
        });

        this.fcm.onTokenRefresh().subscribe(token => {
          // Register your new token in your back-end if you want
          console.log("Refresh token: ", token);
          this.saveTokenInDB(token);
        });

        this.socketService.socketObservable.subscribe((data: SocketEventFormat) => {
          if (data.type === AppConstants.TYPE_LOGOUT) {
            this.logger.debug("Socket Logout Unsubscribe From Topic");
            this.unsubscribeFromTopic();
          }
        });
      }
    });
  }

  saveTokenInDB(token: string) {
    this.apiService.fcmSubcribe({ token: token }).subscribe((res: ResponseFormat) => {
      if (res.success) {
        this.logger.debug("Token res: " + JSON.stringify(res));
      } else {
        this.synergeService.displayAlert(res.message);
      }
    }, error => {
      this.synergeService.displayAlert(error.message);
    });
  }

  subscribeToTopic() {
    this.fcm.subscribeToTopic(localStorage.getItem(AppConstants.USER_ID));
    if (localStorage.getItem(AppConstants.USER_ROLE) === AppConstants.ROLE_ADMIN) {
      this.fcm.subscribeToTopic(localStorage.getItem(AppConstants.USER_ROLE));
      this.fcm.subscribeToTopic(localStorage.getItem(AppConstants.USER_ROLE) + localStorage.getItem(AppConstants.USER_BELONGS_TO));
    }
    this.fcm.subscribeToTopic(localStorage.getItem(AppConstants.USER_BELONGS_TO));
    this.fcm.subscribeToTopic(localStorage.getItem(AppConstants.USER_COMPANY_NAME));
    this.fcm.subscribeToTopic('ALL');
  }

  getToken() {
    this.fcm.getToken().then(token => {
      // Register your new token in your back-end if you want
      console.log("Get token: ", token);
      this.saveTokenInDB(token);
    });
  }

  unsubscribeFromTopic() {
    this.fcm.unsubscribeFromTopic(localStorage.getItem(AppConstants.USER_ID));
    if (localStorage.getItem(AppConstants.USER_ROLE) === AppConstants.ROLE_ADMIN) {
      this.fcm.unsubscribeFromTopic(localStorage.getItem(AppConstants.USER_ROLE));
      this.fcm.unsubscribeFromTopic(localStorage.getItem(AppConstants.USER_ROLE) + localStorage.getItem(AppConstants.USER_BELONGS_TO));
    }
    this.fcm.unsubscribeFromTopic(localStorage.getItem(AppConstants.USER_BELONGS_TO));
    this.fcm.unsubscribeFromTopic(localStorage.getItem(AppConstants.USER_COMPANY_NAME));
    this.fcm.unsubscribeFromTopic('ALL');
  }

}
