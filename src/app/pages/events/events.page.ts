import { Component, OnInit } from '@angular/core';
import { AlertController, IonRouterOutlet, ModalController, PopoverController } from '@ionic/angular';
import { CommentsComponent } from 'src/app/comments/comments.component';
import { ActionPopoverPage } from 'src/app/components/action-popover/action-popover';
import { EventDetailsComponent } from 'src/app/components/event-details/event-details.component';
import { EventComponent } from 'src/app/components/event/event.component';
import { LikesListComponent } from 'src/app/components/likes-list/likes-list.component';
import { AppConstants } from 'src/app/domains/app-constants';
import { Event, Likes } from 'src/app/domains/models/events';
import { ResponseFormat, SocketEventFormat } from 'src/app/domains/response-format';
import { ApiService } from 'src/app/services/api.service';
import { LoggerService } from 'src/app/services/logger.service';
import { SocketService } from 'src/app/services/socket.service';
import { SynergeService } from 'src/app/services/synerge.service';

@Component({
  selector: 'app-events',
  templateUrl: './events.page.html',
  styleUrls: ['./events.page.scss'],
})
export class EventsPage implements OnInit {
  upcomingEvents: Event[];
  completedEvents: Event[];
  displayEvents: Event[];
  selectedSegment = 'Upcoming';

  constructor(
    public synergeService: SynergeService,
    private socketService: SocketService,
    private apiService: ApiService,
    private modalController: ModalController,
    public popoverCtrl: PopoverController,
    private alertController: AlertController,
    public routerOutlet: IonRouterOutlet,
    private logger: LoggerService
  ) { }

  ngOnInit() {
    this.socketService.resetDotNotification();
    this.getAllEvent();
    this.socketService.socketObservable.subscribe((data: SocketEventFormat) => {
      if (data.type === AppConstants.TYPE_EVENT) {
        this.logger.debug("Socket Event component update");
        this.getAllEvent();
      }
    });
  }

  doRefresh(event) {
    this.getAllEvent(event);
  }

  segmentChanged(ev: any) {
    this.selectedSegment = ev.detail.value;
    this.changeDisplayEvents();
  }

  changeDisplayEvents() {
    if (this.selectedSegment === 'Upcoming')
      this.displayEvents = [...this.upcomingEvents];
    else
      this.displayEvents = [...this.completedEvents];
  }

  getAllEvent(event?) {
    this.apiService.getAllEvents().subscribe((res: ResponseFormat) => {
      this.logger.debug("Got Total Events count: " + res.data.length);
      if (event) event.target.complete();
      if (res.success) {
        let totalEvents: Event[] = res.data;
        this.upcomingEvents = totalEvents.filter(x => x.type === "Upcoming");
        this.logger.debug("Upcoming Events count: " + this.upcomingEvents.length);
        this.completedEvents = totalEvents.filter(x => x.type === "Completed");
        this.logger.debug("Completed Events count: " + this.completedEvents.length);
        this.changeDisplayEvents();
      } else {
        this.synergeService.handleError(res.message);
      }
    }, error => {
      if (event) event.target.complete();
      this.synergeService.handleErrorOnAPI(error);
    });
  }

  getDidILike(event: Event) {
    return event.likes.find(x => x.by_user_id === localStorage.getItem(AppConstants.USER_ID));
  }

  async onDetails(id) {
    const modal = await this.modalController.create({
      component: EventDetailsComponent,
      mode: 'ios',
      swipeToClose: true,
      presentingElement: this.routerOutlet.nativeEl,
      componentProps: { eventId: id }
    });
    await modal.present();

    const { data } = await modal.onWillDismiss();
    if (data) {
      this.getAllEvent();
    }
  }

  async onAdd() {
    const modal = await this.modalController.create({
      component: EventComponent
    });
    await modal.present();

    const { data } = await modal.onWillDismiss();
    if (data) {
      this.getAllEvent();
    }
  }

  async presentPopover(e, event) {
    const popover = await this.popoverCtrl.create({
      component: ActionPopoverPage,
      event: e,
      componentProps: {
        showEdit: true,
        showDelete: true
      }
    });
    await popover.present();

    const { data } = await popover.onWillDismiss();
    this.logger.debug("Popover data: " + JSON.stringify(data));
    if (data) {
      if (data.action === 'Edit') {
        this.editEvent(event);
      }
      if (data.action === 'Delete') {
        this.deleteEvent(event);
      }
    }
  }

  async editEvent(event: Event) {
    const modal = await this.modalController.create({
      component: EventComponent,
      componentProps: { eventId: event._id }
    });
    await modal.present();

    const { data } = await modal.onWillDismiss();
    if (data) {
      this.getAllEvent();
    }
  }

  async deleteEvent(event: Event) {
    this.logger.debug("On Delete: " + JSON.stringify(event));
    const alert = await this.alertController.create({
      header: 'Confirm!',
      message: `Do you want to delete ${event.title}?`,
      buttons: [
        {
          text: 'No',
          role: 'cancel',
        }, {
          text: 'Yes',
          handler: () => {
            this.apiService.deleteEventById(event._id).subscribe((res: ResponseFormat) => {
              this.logger.debug("Event Deleted by ID: " + JSON.stringify(res));
              if (res.success) {
                this.getAllEvent();
              } else {
                this.synergeService.handleError(res.message);
              }
            }, error => {
              this.synergeService.handleErrorOnAPI(error);
            });
          }
        }
      ]
    });
    await alert.present();
  }

  onLike(event: Event) {
    if (this.getDidILike(event)) {
      event.likes.splice(event.likes.findIndex(x => x.by_user_id === localStorage.getItem(AppConstants.USER_ID)), 1);
    } else {
      let like: Likes = {
        by_user_id: localStorage.getItem(AppConstants.USER_ID),
        by_user_name: localStorage.getItem(AppConstants.USER_FULL_NAME),
        date: new Date()
      };
      event.likes.push(like);
    }
    this.apiService.updateEventById(event).subscribe((res: ResponseFormat) => {
      this.logger.debug("Update events result: " + JSON.stringify(res));
      if (res.success) {
        this.getAllEvent();
      } else {
        this.synergeService.handleError(res.message);
      }
    }, error => {
      this.synergeService.handleErrorOnAPI(error);
    });
  }

  async onLikeList(event: Event) {
    const modal = await this.modalController.create({
      component: LikesListComponent,
      componentProps: { likes: event.likes }
    });
    await modal.present();
  }

  async onComment(event: Event) {
    const modal = await this.modalController.create({
      component: CommentsComponent,
      componentProps: { event: event }
    });
    await modal.present();

    const { data } = await modal.onWillDismiss();
    if (data) {
      this.getAllEvent();
    }
  }


}
