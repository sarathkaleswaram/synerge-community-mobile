import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import * as md5 from 'md5';
import { User } from 'src/app/domains/models/user';
import { ResponseFormat } from 'src/app/domains/response-format';
import { ApiService } from 'src/app/services/api.service';
import { SynergeService } from 'src/app/services/synerge.service';
import { ValidatorService } from 'src/app/services/validator.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  registerForm: FormGroup;
  buttonText = "Register";
  submitted = false;

  constructor(
    private router: Router,
    private synergeService: SynergeService,
    private apiService: ApiService
  ) { }

  ngOnInit() {
    this.registerForm = new FormGroup({
      first_name: new FormControl('', Validators.required),
      last_name: new FormControl('', Validators.required),
      user_name: new FormControl('', Validators.required),
      email: new FormControl('', [Validators.required, Validators.email]),
      phone: new FormControl('', [Validators.required, ValidatorService.phoneMinLength]),
      newPassword: new FormControl('', [Validators.required, Validators.minLength(6)]),
      confirmPassword: new FormControl('', [Validators.required, ValidatorService.validateNewPassword])
    });
  }

  public hasError = (controlName: string, errorName: string) => {
    return this.registerForm.controls[controlName].hasError(errorName);
  }

  onSubmit() {
    this.submitted = true;
    if (this.registerForm.valid) {
      this.buttonText = "Loading...";
      let user: User = {
        first_name: this.registerForm.get('first_name').value,
        last_name: this.registerForm.get('last_name').value,
        user_name: this.registerForm.get('user_name').value,
        email: this.registerForm.get('email').value,
        phone: this.registerForm.get('phone').value,
        password: md5(this.registerForm.get('confirmPassword').value),
        role: 'CLIENT',
        activated: false,
        deleted: false,
        created_on: new Date(),
        updated_on: new Date(),
      }
      this.apiService.registerUser(user).subscribe((res: ResponseFormat) => {
        if (res.success) {
          this.buttonText = "Success";
          setTimeout(() => {
            this.router.navigate(["/login"]);
          }, 2000);
        } else {
          this.buttonText = "Register";
          this.synergeService.displayAlert(res.message);
        }
      }, error => {
        this.buttonText = "Register";
        this.synergeService.displayAlert(error.message);
      });
    }
  }


}
