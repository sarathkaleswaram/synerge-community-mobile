import { Component, OnInit } from '@angular/core';
import { AppVersion } from '@ionic-native/app-version/ngx';

@Component({
  selector: 'app-about-us',
  templateUrl: './about-us.page.html',
  styleUrls: ['./about-us.page.scss'],
})
export class AboutUsPage implements OnInit {
  version: string;

  constructor(private appVersion: AppVersion) { }

  async ngOnInit() {
    this.version = await this.appVersion.getVersionNumber();
  }

}
