import { Component, OnInit } from '@angular/core';
import { AlertController, ModalController } from '@ionic/angular';
import { AppConstants } from 'src/app/domains/app-constants';
import { Feedback } from 'src/app/domains/models/feedback';
import { ResponseFormat, SocketEventFormat } from 'src/app/domains/response-format';
import { FeedbackComponent } from 'src/app/components/feedback/feedback.component';
import { ApiService } from 'src/app/services/api.service';
import { LoggerService } from 'src/app/services/logger.service';
import { SocketService } from 'src/app/services/socket.service';
import { SynergeService } from 'src/app/services/synerge.service';
import { CommentsComponent } from 'src/app/comments/comments.component';

@Component({
  selector: 'app-feedback',
  templateUrl: './feedback.page.html',
  styleUrls: ['./feedback.page.scss'],
})
export class FeedbackPage implements OnInit {
  feedbacks: Feedback[];

  deleteButton: boolean[] = [];

  constructor(
    public synergeService: SynergeService,
    private apiService: ApiService,
    private socketService: SocketService,
    private modalController: ModalController,
    private alertController: AlertController,
    private logger: LoggerService
  ) { }

  ngOnInit() {
    this.socketService.resetDotNotification();
    this.getFeedbacks();
    this.socketService.socketObservable.subscribe((data: SocketEventFormat) => {
      if (data.type === AppConstants.TYPE_FEEDBACK) {
        this.logger.debug("Socket Feedback component update");
        this.getFeedbacks();
      }
    });
  }

  doRefresh(event) {
    this.getFeedbacks(event);
  }

  getFeedbacks(event?) {
    this.apiService.getAllFeedbacks().subscribe((res: ResponseFormat) => {
      this.logger.debug("Got Feedback count: " + res.data.length);
      if (event) event.target.complete();
      if (res.success) {
        this.feedbacks = res.data;
        this.feedbacks.forEach((feedback, i) => {
          this.deleteButton[i] = false;
        });
      } else {
        this.synergeService.handleError(res.message);
      }
    }, error => {
      if (event) event.target.complete();
      this.synergeService.handleErrorOnAPI(error);
    });
  }

  async onDelete(feedback: Feedback, i: number) {
    this.logger.debug("On Delete: " + JSON.stringify(feedback));
    const alert = await this.alertController.create({
      header: 'Confirm!',
      message: `Do you want to delete ${feedback.feedback}?`,
      buttons: [
        {
          text: 'No',
          role: 'cancel',
        }, {
          text: 'Yes',
          handler: () => {
            this.deleteButton[i] = true;
            this.apiService.deleteFeedbackById(feedback._id).subscribe((res: ResponseFormat) => {
              this.logger.debug("Delete Feedback: " + JSON.stringify(res));
              if (res.success) {
                setTimeout(() => {
                  this.getFeedbacks();
                }, 1000);
              } else {
                this.synergeService.handleError(res.message);
              }
            }, error => {
              this.synergeService.handleErrorOnAPI(error);
            });
          }
        }
      ]
    });
    await alert.present();
  }

  async onAdd() {
    const modal = await this.modalController.create({
      component: FeedbackComponent
    });
    await modal.present();

    const { data } = await modal.onWillDismiss();
    if (data) {
      this.getFeedbacks();
    }
  }

  async editFeedback(feedback: Feedback) {
    const modal = await this.modalController.create({
      component: FeedbackComponent,
      componentProps: { feedbackId: feedback._id }
    });
    await modal.present();

    const { data } = await modal.onWillDismiss();
    if (data) {
      this.getFeedbacks();
    }
  }

  async commentFeedback(feedback: Feedback) {
    const modal = await this.modalController.create({
      component: CommentsComponent,
      componentProps: { feedback: feedback }
    });
    await modal.present();

    const { data } = await modal.onWillDismiss();
    if (data) {
      this.getFeedbacks();
    }
  }

}
