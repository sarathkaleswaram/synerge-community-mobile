import { Component, OnInit, ViewChild } from '@angular/core';
import { ActionSheetController, AlertController, IonRouterOutlet, IonSlides, ModalController } from '@ionic/angular';
import { NewsfeedDetailsComponent } from 'src/app/components/newsfeed-details/newsfeed-details.component';
import { NewsFeedModalComponent } from 'src/app/components/news-feed-modal/news-feed-modal.component';
import { AppConstants } from 'src/app/domains/app-constants';
import { NewsFeed } from 'src/app/domains/models/newsFeed';
import { ResponseFormat } from 'src/app/domains/response-format';
import { ApiService } from 'src/app/services/api.service';
import { LoggerService } from 'src/app/services/logger.service';
import { SynergeService } from 'src/app/services/synerge.service';
import { Carousel } from 'src/app/domains/models/carousel';
import { CarouselComponent } from 'src/app/components/carousel/carousel.component';

@Component({
  selector: 'app-news-feed',
  templateUrl: 'news-feed.page.html',
  styleUrls: ['news-feed.page.scss']
})
export class NewsFeedPage implements OnInit {
  @ViewChild('slider') slider: IonSlides;
  newsFeeds: NewsFeed[];
  carousels: Carousel[];
  slideOptsOne = {
    initialSlide: 0,
    slidesPerView: 1,
    autoplay: true
  };

  constructor(
    private actionSheetController: ActionSheetController,
    public alertController: AlertController,
    public synergeService: SynergeService,
    private apiService: ApiService,
    private logger: LoggerService,
    public routerOutlet: IonRouterOutlet,
    private modalController: ModalController
  ) { }

  ngOnInit() {
    this.getAllNewsFeed();
    this.getCarousel();
  }

  doRefresh(event) {
    this.getAllNewsFeed(event);
    this.getCarousel(event);
  }

  getAllNewsFeed(event?) {
    this.apiService.getAllNewsFeeds().subscribe((res: ResponseFormat) => {
      this.logger.debug("Got News Feed count: " + res.data.length);
      if (event) event.target.complete();
      if (res.success) {
        this.newsFeeds = res.data;
      } else {
        this.synergeService.handleError(res.message);
      }
    }, error => {
      if (event) event.target.complete();
      this.synergeService.handleErrorOnAPI(error);
    });
  }

  getCarousel(event?) {
    this.apiService.getAllCarousels().subscribe((res: ResponseFormat) => {
      this.logger.debug("Got Carousel count: " + res.data.length);
      if (event) event.target.complete();
      if (res.success) {
        this.carousels = res.data;
      } else {
        this.synergeService.handleError(res.message);
      }
    }, error => {
      if (event) event.target.complete();
      this.synergeService.handleErrorOnAPI(error);
    });
  }

  getDidILike(newsFeed: NewsFeed) {
    return newsFeed.likes.find(x => x.by_user_id === localStorage.getItem(AppConstants.USER_ID));
  }

  async onAdd() {
    if (this.synergeService.isAdmin) {
      const actionSheet = await this.actionSheetController.create({
        buttons: [{
          text: 'Add News Feed',
          handler: () => {
            this.onAddNewsFeed();
          }
        },
        {
          text: 'Add Announcement',
          handler: () => {
            this.onAddCarousel();
          }
        },
        {
          text: 'Cancel',
          role: 'cancel'
        }]
      });
      await actionSheet.present();
    } else {
      this.onAddNewsFeed();
    }
  }

  async onAddCarousel() {
    const modal = await this.modalController.create({
      component: CarouselComponent
    });
    await modal.present();

    const { data } = await modal.onWillDismiss();
    if (data) {
      this.getCarousel();
    }
  }

  async onAddNewsFeed() {
    const modal = await this.modalController.create({
      component: NewsFeedModalComponent
    });
    await modal.present();

    const { data } = await modal.onWillDismiss();
    if (data) {
      this.getAllNewsFeed();
    }
  }

  async onDetails(id) {
    const modal = await this.modalController.create({
      component: NewsfeedDetailsComponent,
      mode: 'ios',
      swipeToClose: true,
      presentingElement: this.routerOutlet.nativeEl,
      componentProps: { id: id }
    });
    await modal.present();

    const { data } = await modal.onWillDismiss();
    if (data) {
      this.getAllNewsFeed();
    }
  }

  async onCarouselPress(id) {
    const actionSheet = await this.actionSheetController.create({
      buttons: [{
        text: 'Edit',
        handler: async () => {
          this.onEditCarousel(id);
        }
      },
      {
        text: 'Delete',
        handler: async () => {
          this.onDeleteCarousel(id);
        }
      },
      {
        text: 'Cancel',
        role: 'cancel'
      }]
    });
    await actionSheet.present();
  }

  async onEditCarousel(id) {
    const modal = await this.modalController.create({
      component: CarouselComponent,
      componentProps: { carouselId: id }
    });
    await modal.present();

    const { data } = await modal.onWillDismiss();
    if (data) {
      this.getCarousel();
    }
  }

  async onDeleteCarousel(id) {
    const alert = await this.alertController.create({
      header: 'Confirm!',
      message: `Do you want to delete Announcement?`,
      buttons: [
        {
          text: 'No',
          role: 'cancel',
        }, {
          text: 'Yes',
          handler: () => {
            this.apiService.deleteCarouselById(id).subscribe((res: ResponseFormat) => {
              this.logger.debug("Carousel Deleted by ID: " + JSON.stringify(res));
              if (res.success) {
                this.getCarousel();
              } else {
                this.synergeService.handleError(res.message);
              }
            }, error => {
              this.synergeService.handleErrorOnAPI(error);
            });
          }
        }
      ]
    });
    await alert.present();
  }

}
