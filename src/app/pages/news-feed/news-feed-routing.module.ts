import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NewsFeedPage } from './news-feed.page';

const routes: Routes = [
  {
    path: '',
    component: NewsFeedPage,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NewsFeedPageRoutingModule {}
