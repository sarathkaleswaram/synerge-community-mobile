import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NewsFeedPage } from './news-feed.page';
import { ExploreContainerComponentModule } from '../explore-container/explore-container.module';

import { NewsFeedPageRoutingModule } from './news-feed-routing.module';
import { ComponentsModule } from 'src/app/components/components.module';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    ExploreContainerComponentModule,
    NewsFeedPageRoutingModule,
    ComponentsModule
  ],
  declarations: [NewsFeedPage]
})
export class NewsFeedPageModule {}
