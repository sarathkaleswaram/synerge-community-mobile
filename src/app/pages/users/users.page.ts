import { Component, OnInit } from '@angular/core';
import { AlertController, ModalController } from '@ionic/angular';
import { AppConstants } from 'src/app/domains/app-constants';
import { User } from 'src/app/domains/models/user';
import { ResponseFormat } from 'src/app/domains/response-format';
import { ApiService } from 'src/app/services/api.service';
import { LoggerService } from 'src/app/services/logger.service';
import { SocketService } from 'src/app/services/socket.service';
import { SynergeService } from 'src/app/services/synerge.service';
import { SelectClientCompanyComponent } from 'src/app/components/select-client-company/select-client-company.component';
import { EditProfileComponent } from 'src/app/components/edit-profile/edit-profile.component';

@Component({
  selector: 'app-users',
  templateUrl: './users.page.html',
  styleUrls: ['./users.page.scss'],
})
export class UsersPage implements OnInit {
  users: User[] = [];
  activateButtonText: string[] = [];
  deleteButtonText: string[] = [];
  roleAdmin = AppConstants.ROLE_ADMIN;

  constructor(private apiService: ApiService,
    private socketService: SocketService,
    public synergeService: SynergeService,
    private modalController: ModalController,
    private alertController: AlertController,
    private logger: LoggerService
  ) { }

  ngOnInit() {
    this.getUsersList();
  }

  doRefresh(event) {
    this.getUsersList(event);
  }

  getUsersList(event?) {
    this.socketService.resetDotNotification();
    this.apiService.getAllUsers().subscribe((res: ResponseFormat) => {
      this.logger.debug("Got users count: " + res.data.length);
      if (event) event.target.complete();
      this.users = res.data;
      this.users.forEach((user: User, i: number) => {
        user.activated ? this.activateButtonText[i] = 'Inactivate' : this.activateButtonText[i] = 'Activate';
        this.deleteButtonText[i] = 'Delete';
      });
    }, error => {
      if (event) event.target.complete();
      this.synergeService.handleErrorOnAPI(error);
    });
  }

  async onUserClick(user: User) {    
    const modal = await this.modalController.create({
      component: EditProfileComponent,
      componentProps: { id: user._id }
    });
    await modal.present();

    const { data } = await modal.onWillDismiss();
    if (data) {
      this.getUsersList();
    }
  }

  async onActivate(user: User, i: number) {
    this.logger.debug("On Activate: " + JSON.stringify(user));
    if (user.belongs_to) {
      if (user.activated) {
        user.activated = false;
      } else {
        user.activated = true;
      }
      this.activateButtonText[i] = "Loading...";
      this.apiService.updateUser(user).subscribe((res: ResponseFormat) => {
        this.logger.debug("Update Profile - User by ID: " + JSON.stringify(res));
        if (res.success) {
          setTimeout(() => {
            this.getUsersList();
          }, 1000);
        } else {
          this.synergeService.handleError(res.message);
        }
      }, error => {
        this.synergeService.handleErrorOnAPI(error);
      });
    } else {
      const modal = await this.modalController.create({
        component: SelectClientCompanyComponent
      });
      await modal.present();

      const { data } = await modal.onWillDismiss();
      if (data) {
        user.belongs_to = data.belongs_to;
        user.company_name = data.company_name;
        this.onActivate(user, i);
      }
    }
  }

  async onDelete(user: User, i: number) {
    this.logger.debug("On Delete: " + JSON.stringify(user));
    const alert = await this.alertController.create({
      header: 'Confirm!',
      message: `Do you want to delete ${user.first_name} ${user.last_name}?`,
      buttons: [
        {
          text: 'No',
          role: 'cancel',
        }, {
          text: 'Yes',
          handler: () => {
            this.deleteButtonText[i] = "Loading...";
            this.apiService.deleteUser(user).subscribe((res: ResponseFormat) => {
              this.logger.debug("Update Profile - User by ID: " + JSON.stringify(res));
              if (res.success) {
                setTimeout(() => {
                  this.getUsersList();
                }, 1000);
              } else {
                this.synergeService.handleError(res.message);
              }
            }, error => {
              this.synergeService.handleErrorOnAPI(error);
            });
          }
        }
      ]
    });
    await alert.present();
  }


}
