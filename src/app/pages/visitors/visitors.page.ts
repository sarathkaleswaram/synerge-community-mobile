import { Component, OnInit } from '@angular/core';
import { AlertController, IonRouterOutlet, ModalController } from '@ionic/angular';
import { AddVisitorComponent } from 'src/app/components/add-visitor/add-visitor.component';
import { AppConstants } from 'src/app/domains/app-constants';
import { ClientVisitors } from 'src/app/domains/models/clientVisitors';
import { ResponseFormat, SocketEventFormat } from 'src/app/domains/response-format';
import { ApiService } from 'src/app/services/api.service';
import { LoggerService } from 'src/app/services/logger.service';
import { SocketService } from 'src/app/services/socket.service';
import { SynergeService } from 'src/app/services/synerge.service';

@Component({
  selector: 'app-visitors',
  templateUrl: 'visitors.page.html',
  styleUrls: ['visitors.page.scss']
})
export class VisitorsPage implements OnInit {
  clientVisitors: ClientVisitors[];
  deleteButtonText: string[] = [];

  constructor(
    public synergeService: SynergeService,
    private socketService: SocketService,
    private apiService: ApiService,
    public alertController: AlertController,
    private modalController: ModalController,
    public routerOutlet: IonRouterOutlet,
    private logger: LoggerService
  ) { }

  ngOnInit() {
    this.socketService.resetDotNotification();
    this.getClientVisitors();
    this.socketService.socketObservable.subscribe((data: SocketEventFormat) => {
      if (data.type === AppConstants.TYPE_CLIENT_VISITOR) {
        this.logger.debug("Socket ClientVisitors component update");
        this.getClientVisitors();
      }
    });
  }

  doRefresh(event) {
    this.getClientVisitors(event);
  }

  getClientVisitors(event?) {
    this.apiService.getAllClientVisitors().subscribe((res: ResponseFormat) => {
      this.logger.debug("Got Client Visitors count: " + res.data.length);
      if (event) event.target.complete();
      if (res.success) {
        this.clientVisitors = res.data;
        this.clientVisitors.forEach((clientVistors: ClientVisitors, i: number) => {
          this.deleteButtonText[i] = 'Delete';
        });
      } else {
        this.synergeService.handleError(res.message);
      }
    }, error => {
      if (event) event.target.complete();
      this.synergeService.handleErrorOnAPI(error);
    });
  }

  async onDelete(clientVistors: ClientVisitors, i: number) {
    this.logger.debug("On Delete: " + JSON.stringify(clientVistors) + ", i: " + i);
    const alert = await this.alertController.create({
      header: 'Confirm!',
      message: `Do you want to delete visitor ${clientVistors.visitor_name} ? `,
      buttons: [
        {
          text: 'No',
          role: 'cancel',
        }, {
          text: 'Yes',
          handler: () => {
            this.deleteButtonText[i] = "Loading...";
            this.apiService.deleteClientVisitorById(clientVistors).subscribe((res: ResponseFormat) => {
              this.logger.debug("Client Vistors Deleted by ID: " + JSON.stringify(res));
              if (res.success) {
                setTimeout(() => {
                  this.getClientVisitors();
                }, 1000);
              } else {
                this.synergeService.handleError(res.message);
              }
            }, error => {
              this.synergeService.handleErrorOnAPI(error);
            });
          }
        }
      ]
    });
    await alert.present();
  }

  async onAdd() {
    const modal = await this.modalController.create({
      component: AddVisitorComponent,
      mode: 'ios',
      swipeToClose: true,
      presentingElement: this.routerOutlet.nativeEl,
    });
    await modal.present();

    const { data } = await modal.onWillDismiss();
    if (data) {
      this.getClientVisitors();
    }
  }

}
