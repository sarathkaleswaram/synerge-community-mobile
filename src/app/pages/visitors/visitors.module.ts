import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { VisitorsPage } from './visitors.page';
import { ExploreContainerComponentModule } from '../explore-container/explore-container.module';

import { VisitorsPageRoutingModule } from './visitors-routing.module';
import { ComponentsModule } from 'src/app/components/components.module';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    ExploreContainerComponentModule,
    VisitorsPageRoutingModule,
    ComponentsModule
  ],
  declarations: [VisitorsPage]
})
export class VisitorsPageModule {}
