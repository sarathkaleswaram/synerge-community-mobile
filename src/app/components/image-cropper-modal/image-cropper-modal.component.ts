import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ImageCroppedEvent } from 'ngx-image-cropper';

@Component({
  selector: 'app-image-cropper-modal',
  templateUrl: './image-cropper-modal.component.html',
  styleUrls: ['./image-cropper-modal.component.scss'],
})
export class ImageCropperComponentModal implements OnInit {
  @Input() imageEvent: any;
  @Input() aspectRatio;

  imageChangedEvent: any = '';
  croppedImage: any = '';

  uploadedFile: any;
  croppedFile: any;

  constructor(
    private modalController: ModalController
  ) { }

  ngOnInit() {
    if (!this.aspectRatio)
      this.aspectRatio = 4 / 4;
    this.imageChangedEvent = this.imageEvent;
    this.uploadedFile = this.imageEvent.target.files[0];
  }

  imageCropped(event: ImageCroppedEvent) {
    this.croppedImage = event.base64;
  }

  base64ToFile(data, filename) {
    const arr = data.split(',');
    const mime = arr[0].match(/:(.*?);/)[1];
    const bstr = atob(arr[1]);
    let n = bstr.length;
    let u8arr = new Uint8Array(n);
    while (n--) {
      u8arr[n] = bstr.charCodeAt(n);
    }
    return new File([u8arr], filename, { type: mime });
  }

  imageLoaded() {
    // show cropper
  }

  cropperReady() {
    // cropper ready
  }

  loadImageFailed() {
    // show message
  }

  submitCrop() {
    // Data URI (base64) to File to send backend
    this.croppedFile = this.base64ToFile(this.croppedImage, this.uploadedFile.name);
    this.modalController.dismiss({ croppedImage: this.croppedImage, croppedFile: this.croppedFile });
  }

  dismiss() {
    this.modalController.dismiss();
  }

}
