import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { AppConstants } from 'src/app/domains/app-constants';
import { Carousel } from 'src/app/domains/models/carousel';
import { ResponseFormat } from 'src/app/domains/response-format';
import { ApiService } from 'src/app/services/api.service';
import { LoggerService } from 'src/app/services/logger.service';
import { SynergeService } from 'src/app/services/synerge.service';

@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.scss'],
})
export class CarouselComponent implements OnInit {
  @Input() carouselId: string;
  carousel: Carousel = {
    by_user_id: undefined,
    by_user_name: undefined,
    date: undefined,
    image_path: undefined,
  };
  imgURL;
  imagePath;
  buttonText = "Save";

  constructor(
    private modalController: ModalController,
    public apiService: ApiService,
    public synergeService: SynergeService,
    private logger: LoggerService,
  ) { }

  ngOnInit() {
    if (this.carouselId) {
      this.logger.debug("Edit carousel ID: " + this.carouselId);
      this.apiService.getCarouselById(this.carouselId).subscribe(data => {
        this.logger.debug("Edit carousel ID: " + JSON.stringify(data));
        this.carousel = data.data;
        this.imgURL = this.synergeService.basePath + this.carousel.image_path;
      }, error => {
        this.synergeService.handleErrorOnAPI(error);
      });
    }
  }

  onUpload(croppedImage, croppedFile) {
    this.imgURL = croppedImage;
    this.imagePath = croppedFile;
  }

  onSave() {
    if (this.imagePath) {
      this.buttonText = "Loading...";
      let formData: FormData = new FormData();
      if (this.synergeService.isMobile) {
        let file = this.synergeService.dataURItoBlob(this.imgURL);
        formData.append('uploadImage', file, `carousel-${(new Date().getTime()).toString(16)}.png`);
      } else {
        formData.append('uploadImage', this.imagePath);
      }
      this.apiService.uploadImage(formData).subscribe((res: ResponseFormat) => {
        if (res.success) {
          this.carousel.image_path = '/uploads/' + res.data;
          this.saveContent();
        }
      }, error => {
        this.buttonText = "Save";
        this.synergeService.handleErrorOnAPI(error);
      });
    } else {
      this.synergeService.displayAlert("Please upload image.");
    }
  }

  saveContent() {
    this.buttonText = "Loading...";
    if (this.carouselId) {
      this.apiService.updateCarouselById(this.carousel).subscribe((res: ResponseFormat) => {
        this.logger.debug("Carousel: " + JSON.stringify(res));
        if (res.success) {
          this.buttonText = "Success";
          setTimeout(() => {
            this.modalController.dismiss("Success");
          }, 1000);
        } else {
          this.buttonText = "Save";
          this.synergeService.displayAlert(res.message);
        }
      }, error => {
        this.buttonText = "Save";
        this.synergeService.handleErrorOnAPI(error);
      });
    } else {
      this.carousel.date = new Date();
      this.carousel.by_user_id = localStorage.getItem(AppConstants.USER_ID);
      this.carousel.by_user_name = localStorage.getItem(AppConstants.USER_FULL_NAME);
      this.apiService.saveCarousel(this.carousel).subscribe((res: ResponseFormat) => {
        this.logger.debug("Carousel: " + JSON.stringify(res));
        if (res.success) {
          this.buttonText = "Success";
          setTimeout(() => {
            this.modalController.dismiss("Success");
          }, 1000);
        } else {
          this.buttonText = "Save";
          this.synergeService.displayAlert(res.message);
        }
      }, error => {
        this.buttonText = "Save";
        this.synergeService.handleErrorOnAPI(error);
      });
    }
  }

  dismiss() {
    this.modalController.dismiss();
  }

}
