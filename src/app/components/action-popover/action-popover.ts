import { Component, EventEmitter, Input, Output } from '@angular/core';

import { PopoverController } from '@ionic/angular';
import { SynergeService } from 'src/app/services/synerge.service';

@Component({
  template: `
    <ion-list>
      <ion-item button (click)="close('Edit')" *ngIf="showEdit">
        <ion-label>Edit</ion-label>
      </ion-item>
      <ion-item button (click)="close('Delete')" *ngIf="showDelete">
        <ion-label>Delete</ion-label>
      </ion-item>
      <ion-item button (click)="close('ChangePhoto')" *ngIf="showChangePhoto">
        <app-image [onButton]="false" [buttonText]="'Upload Image'" (onUploadEvent)="onAppUpload($event[0], $event[1])"></app-image>
      </ion-item>
      <ion-item button (click)="close('RemovePhoto')" *ngIf="showRemovePhoto">
        <ion-label>Remove Photo</ion-label>
      </ion-item>
    </ion-list>
  `
})
export class ActionPopoverPage {
  @Input() showEdit: boolean;
  @Input() showDelete: boolean;
  @Input() showChangePhoto: boolean;
  @Input() showRemovePhoto: boolean;

  constructor(
    public popoverCtrl: PopoverController,
    public synergeService: SynergeService
  ) { }

  close(action: string) {
    if (action !== 'ChangePhoto') {
      this.popoverCtrl.dismiss({ action });
    }
  }

  onAppUpload(croppedImage, croppedFile) {
    this.popoverCtrl.dismiss({ action: 'ChangePhoto', data: { croppedImage, croppedFile } });
  }
}
