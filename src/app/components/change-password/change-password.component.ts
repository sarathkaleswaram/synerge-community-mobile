import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ModalController } from '@ionic/angular';
import { User } from 'src/app/domains/models/user';
import { ApiService } from 'src/app/services/api.service';
import { LoggerService } from 'src/app/services/logger.service';
import { SynergeService } from 'src/app/services/synerge.service';
import { ValidatorService } from 'src/app/services/validator.service';
import * as md5 from 'md5';
import { ResponseFormat } from 'src/app/domains/response-format';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss'],
})
export class ChangePasswordComponent implements OnInit {
  @Input() user: User;
  submitted = false;
  changePasswordForm: FormGroup;
  buttonText = "Confirm";

  constructor(
    private formBuilder: FormBuilder,
    private modalController: ModalController,
    public apiService: ApiService,
    public synergeService: SynergeService,
    private logger: LoggerService
  ) { }

  ngOnInit() {    
    this.changePasswordForm = this.formBuilder.group({
      oldPassword: new FormControl(this.user.password),
      currentPassword: new FormControl('', [Validators.required, ValidatorService.validateOldPassword]),
      newPassword: new FormControl('', [Validators.required, Validators.minLength(6)]),
      confirmPassword: new FormControl('', [Validators.required, ValidatorService.validateNewPassword])
    });
  }

  onSubmit() {
    this.submitted = true;
    if (this.changePasswordForm.valid) {
      this.buttonText = "Loading...";
      this.user.password = md5(this.changePasswordForm.value.newPassword);
      this.apiService.updateUser(this.user).subscribe((res: ResponseFormat) => {
        this.logger.debug("Change Password: " + JSON.stringify(res));
        if (res.success) {
          this.buttonText = "Success";
          setTimeout(() => {
            this.modalController.dismiss(res.data);
          }, 1000);
        } else {
          this.buttonText = "Confirm";
          this.synergeService.handleError(res.message);
        }
      }, error => {
        this.buttonText = "Confirm";
        this.synergeService.handleErrorOnAPI(error);
      });
    }
  }

  public hasError = (controlName: string, errorName: string) => {
    return this.changePasswordForm.controls[controlName].hasError(errorName);
  }

  dismiss() {
    this.modalController.dismiss();
  }

}
