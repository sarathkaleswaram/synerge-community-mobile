import { Component, Input, OnInit } from '@angular/core';
import { ActionSheetController } from '@ionic/angular';
import { SynergeService } from 'src/app/services/synerge.service';

@Component({
  selector: 'app-share-action',
  templateUrl: './share-action.component.html',
  styleUrls: ['./share-action.component.scss'],
})
export class ShareActionComponent implements OnInit {
  @Input() text: any;
  @Input() type: string;
  @Input() position: string;

  showLeft = true;

  constructor(
    public synergeService: SynergeService,
    private actionSheetController: ActionSheetController
  ) { 
    this.showLeft = this.position === 'end' ? true : false;
  }

  ngOnInit() { }

  async onPress() {
    let buttons: any = [{
      text: 'Copy',
      handler: () => {
        this.synergeService.copyToClipboard(this.text);
      }
    }];

    if (this.type.includes('mail')) {
      buttons.push({
        text: `Send mail to: ${this.text}`,
        handler: () => {
          window.location.href = `mailto:${this.text}`;
        }
      })
    }
    if (this.type.includes('phone')) {
      buttons.push({
        text: `Call: ${this.text}`,
        handler: () => {
          window.location.href = `tel:${this.text}`;
        }
      })
    }

    buttons.push({
      text: 'Cancel',
      role: 'cancel'
    });
    const actionSheet = await this.actionSheetController.create({
      cssClass: 'brand-action-sheet',
      buttons: buttons
    });
    await actionSheet.present();
  }

}
