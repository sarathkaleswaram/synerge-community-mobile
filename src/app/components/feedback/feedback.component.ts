import { Component, Input, OnInit } from '@angular/core';
import { AlertController, ModalController } from '@ionic/angular';
import { AppConstants } from 'src/app/domains/app-constants';
import { Comments, Feedback } from 'src/app/domains/models/feedback';
import { ResponseFormat } from 'src/app/domains/response-format';
import { ApiService } from 'src/app/services/api.service';
import { LoggerService } from 'src/app/services/logger.service';
import { SynergeService } from 'src/app/services/synerge.service';

@Component({
  selector: 'app-feedback',
  templateUrl: './feedback.component.html',
  styleUrls: ['./feedback.component.scss'],
})
export class FeedbackComponent implements OnInit {
  @Input() feedbackId: string;
  feedback: Feedback = {
    by_user_id: undefined,
    by_user_name: undefined,
    feedback: "",
    requested_date: undefined,
    belongs_to: undefined,
    company_name: undefined,
    comments: [],
    status: "Not Seen"
  };
  buttonText = "Submit";
  doUpdate = false;


  constructor(
    private modalController: ModalController,
    public apiService: ApiService,
    public alertController: AlertController,
    public synergeService: SynergeService,
    private logger: LoggerService
  ) { }

  ngOnInit() {
    this.getFeedback();
  }

  getFeedback() {
    if (this.feedbackId) {
      this.apiService.getFeedbackById(this.feedbackId).subscribe(data => {
        this.logger.debug("Edit Feedback - Feedback by ID: " + JSON.stringify(data));
        this.feedback = data.data;
      }, error => {
        this.synergeService.handleErrorOnAPI(error);
      });
    }
  }

  onSubmit() {
    if (this.feedback.feedback == "") {
      this.synergeService.displayAlert("Please enter all fields.");
      return;
    }
    if (this.feedbackId) {
      this.apiService.updateFeedbackById(this.feedback).subscribe((res: ResponseFormat) => {
        this.logger.debug("Feedback Updated data: " + JSON.stringify(res));
        if (res.success) {
          this.buttonText = "Success";
          setTimeout(() => {
            this.modalController.dismiss("update");
          }, 1000);
        } else {
          this.buttonText = "Submit";
          this.synergeService.handleError(res.message);
        }
      }, error => {
        this.buttonText = "Submit";
        this.synergeService.handleErrorOnAPI(error);
      });
    } else {
      let belongs_to: any = localStorage.getItem(AppConstants.USER_BELONGS_TO);
      let feedback: Feedback = {
        by_user_id: localStorage.getItem(AppConstants.USER_ID),
        by_user_name: localStorage.getItem(AppConstants.USER_FULL_NAME),
        feedback: this.feedback.feedback,
        requested_date: new Date(),
        belongs_to: belongs_to,
        company_name: localStorage.getItem(AppConstants.USER_COMPANY_NAME),
        comments: [],
        status: "Not Seen"
      };
      this.apiService.createFeedback(feedback).subscribe((res: ResponseFormat) => {
        this.logger.debug("Feedback Saved data: " + JSON.stringify(res));
        if (res.success) {
          this.buttonText = "Success";
          setTimeout(() => {
            this.modalController.dismiss("update");
          }, 1000);
        } else {
          this.buttonText = "Submit";
          this.synergeService.handleError(res.message);
        }
      }, error => {
        this.buttonText = "Submit";
        this.synergeService.handleErrorOnAPI(error);
      });
    }
  }

  async onAddComment() {
    const alert = await this.alertController.create({
      header: 'Enter Comment',
      inputs: [
        {
          name: 'comment',
          id: 'comment',
          type: 'textarea',
          placeholder: 'Enter Comment'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Ok',
          handler: (data) => {
            if (data.comment) {
              let comment: Comments = {
                comment: data.comment,
                by_user_id: localStorage.getItem(AppConstants.USER_ID),
                by_user_name: localStorage.getItem(AppConstants.USER_FULL_NAME),
                date: new Date()
              };
              this.feedback.comments.push(comment);
              this.apiService.updateFeedbackById(this.feedback).subscribe((res: ResponseFormat) => {
                this.logger.debug("Update feedback result: " + JSON.stringify(res));
                if (res.success) {
                  this.doUpdate = true;
                  this.getFeedback();
                } else {
                  this.synergeService.handleError(res.message);
                }
              }, error => {
                this.synergeService.handleErrorOnAPI(error);
              });
            }
          }
        }
      ]
    });

    await alert.present();
  }

  dismiss() {
    if (this.doUpdate)
      this.modalController.dismiss("update");
    else
      this.modalController.dismiss();
  }

}
