import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HeaderComponent } from './header/header.component';
import { SpinnerComponent } from './spinner/spinner.component';
import { NewsFeedModalComponent } from './news-feed-modal/news-feed-modal.component';
// import { Camera } from '@ionic-native/camera/ngx';
// import { File } from '@ionic-native/file/ngx';
import { NewsfeedDetailsComponent } from './newsfeed-details/newsfeed-details.component';
import { BookingDetailsComponent } from './booking-details/booking-details.component';
import { SelectClientCompanyComponent } from './select-client-company/select-client-company.component';
import { AddBookingComponent } from './add-booking/add-booking.component';
import { BookingTimeComponent } from './booking-time/booking-time.component';
import { ActionPopoverPage } from './action-popover/action-popover';
import { AddVisitorComponent } from './add-visitor/add-visitor.component';
import { EditProfileComponent } from './edit-profile/edit-profile.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { ImageComponent } from './image/image.component';
import { ImageCropperModule } from 'ngx-image-cropper';
import { ImageCropperComponentModal } from './image-cropper-modal/image-cropper-modal.component';
import { ClientComponent } from './client/client.component';
import { EventComponent } from './event/event.component';
import { LikesListComponent } from './likes-list/likes-list.component';
import { CommentsComponent } from '../comments/comments.component';
import { FeedbackComponent } from './feedback/feedback.component';
import { AchieverComponent } from './achiever/achiever.component';
import { SynergeFamilyComponent } from './synerge-family/synerge-family.component';
// import { Crop } from '@ionic-native/crop/ngx';
import { UserInfoComponent } from './user-info/user-info.component';
import { ShareActionComponent } from './share-action/share-action.component';
import { EventDetailsComponent } from './event-details/event-details.component';
import { AchieverDetailsComponent } from './achiever-details/achiever-details.component';
import { CarouselComponent } from './carousel/carousel.component';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ImageCropperModule,
  ],
  exports: [
    HeaderComponent,
    SpinnerComponent,
    NewsFeedModalComponent,
    NewsfeedDetailsComponent,
    BookingDetailsComponent,
    SelectClientCompanyComponent,
    AddBookingComponent,
    BookingTimeComponent,
    ActionPopoverPage,
    AddVisitorComponent,
    EditProfileComponent,
    ChangePasswordComponent,
    ImageComponent,
    ImageCropperComponentModal,
    ClientComponent,
    EventComponent,
    LikesListComponent,
    CommentsComponent,
    FeedbackComponent,
    AchieverComponent,
    SynergeFamilyComponent,
    UserInfoComponent,
    ShareActionComponent,
    EventDetailsComponent,
    AchieverDetailsComponent,
    CarouselComponent,
  ],
  declarations: [
    HeaderComponent,
    SpinnerComponent,
    NewsFeedModalComponent,
    NewsfeedDetailsComponent,
    BookingDetailsComponent,
    SelectClientCompanyComponent,
    AddBookingComponent,
    BookingTimeComponent,
    ActionPopoverPage,
    AddVisitorComponent,
    EditProfileComponent,
    ChangePasswordComponent,
    ImageComponent,
    ImageCropperComponentModal,
    ClientComponent,
    EventComponent,
    LikesListComponent,
    CommentsComponent,
    FeedbackComponent,
    AchieverComponent,
    SynergeFamilyComponent,
    UserInfoComponent,
    ShareActionComponent,
    EventDetailsComponent,
    AchieverDetailsComponent,
    CarouselComponent,
  ],
  entryComponents: [
    HeaderComponent,
    SpinnerComponent,
    NewsFeedModalComponent,
    NewsfeedDetailsComponent,
    BookingDetailsComponent,
    SelectClientCompanyComponent,
    AddBookingComponent,
    BookingTimeComponent,
    ActionPopoverPage,
    AddVisitorComponent,
    EditProfileComponent,
    ChangePasswordComponent,
    ImageComponent,
    ImageCropperComponentModal,
    ClientComponent,
    EventComponent,
    LikesListComponent,
    CommentsComponent,
    FeedbackComponent,
    AchieverComponent,
    SynergeFamilyComponent,
    UserInfoComponent,
    ShareActionComponent,
    EventDetailsComponent,
    AchieverDetailsComponent,
    CarouselComponent,
  ],
  // providers: [Camera, File, Crop],
})
export class ComponentsModule { }
