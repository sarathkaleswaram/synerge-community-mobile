import { Component, Input, OnInit } from '@angular/core';
import { AlertController, ModalController, PopoverController } from '@ionic/angular';
import { Achiever } from 'src/app/domains/models/achievers';
import { ResponseFormat } from 'src/app/domains/response-format';
import { ApiService } from 'src/app/services/api.service';
import { LoggerService } from 'src/app/services/logger.service';
import { SynergeService } from 'src/app/services/synerge.service';
import { AchieverComponent } from '../achiever/achiever.component';
import { ActionPopoverPage } from '../action-popover/action-popover';

@Component({
  selector: 'app-achiever-details',
  templateUrl: './achiever-details.component.html',
  styleUrls: ['./achiever-details.component.scss'],
})
export class AchieverDetailsComponent implements OnInit {
  @Input() achieverId;
  achiever: Achiever;
  doUpdate = false;

  constructor(
    private apiService: ApiService,
    public synergeService: SynergeService,
    private logger: LoggerService,
    private modalController: ModalController,
    public alertController: AlertController,
    public popoverCtrl: PopoverController
  ) { }

  ngOnInit() {
    this.getAchiever();
  }

  getAchiever() {
    this.apiService.getAchieverById(this.achieverId).subscribe(data => {
      this.logger.debug("Achiever by ID: " + JSON.stringify(data));
      this.achiever = data.data;
    }, error => {
      this.synergeService.handleErrorOnAPI(error);
    });
  }

  async presentPopover(event) {
    const popover = await this.popoverCtrl.create({
      component: ActionPopoverPage,
      event,
      componentProps: {
        showEdit: true,
        showDelete: true
      }
    });
    await popover.present();

    const { data } = await popover.onWillDismiss();
    this.logger.debug("Popover data: " + JSON.stringify(data));
    if (data) {
      if (data.action === 'Edit') {
        this.editAchiever(this.achiever);
      }
      if (data.action === 'Delete') {
        this.deleteAchiever(this.achiever);
      }
    }
  }

  async editAchiever(achiever: Achiever) {
    const modal = await this.modalController.create({
      component: AchieverComponent,
      componentProps: { achieverId: achiever._id }
    });
    await modal.present();

    const { data } = await modal.onWillDismiss();
    if (data) {
      this.doUpdate = true;
      this.getAchiever();
    }
  }

  async deleteAchiever(achiever: Achiever) {
    this.logger.debug("On Delete: " + JSON.stringify(achiever));
    const alert = await this.alertController.create({
      header: 'Confirm!',
      message: `Do you want to delete ${achiever.achiever}?`,
      buttons: [
        {
          text: 'No',
          role: 'cancel',
        }, {
          text: 'Yes',
          handler: () => {
            this.apiService.deleteAchieverById(achiever._id).subscribe((res: ResponseFormat) => {
              this.logger.debug("Achiever Deleted by ID: " + JSON.stringify(res));
              if (res.success) {
                setTimeout(() => {
                  this.modalController.dismiss("update");
                }, 1000);
              } else {
                this.synergeService.handleError(res.message);
              }
            }, error => {
              this.synergeService.handleErrorOnAPI(error);
            });
          }
        }
      ]
    });
    await alert.present();
  }


  dismiss() {
    if (this.doUpdate)
      this.modalController.dismiss("update");
    else
      this.modalController.dismiss();
  }


}
