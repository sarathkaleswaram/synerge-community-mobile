import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ModalController } from '@ionic/angular';
import { Clients } from 'src/app/domains/models/clients';
import { ResponseFormat } from 'src/app/domains/response-format';
import { ApiService } from 'src/app/services/api.service';
import { LoggerService } from 'src/app/services/logger.service';
import { SynergeService } from 'src/app/services/synerge.service';

@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.scss'],
})
export class ClientComponent implements OnInit {
  @Input() clientId: string;

  clientForm: FormGroup;
  client: Clients = {
    active_status: undefined,
    belongs_to: undefined,
    company_name: undefined,
    joined_date: undefined
  };
  buttonText = "Confirm";
  dialogHeader: string;
  submitted = false;

  constructor(
    private formBuilder: FormBuilder,
    private apiService: ApiService,
    public synergeService: SynergeService,
    private modalController: ModalController,
    private logger: LoggerService
  ) { }

  ngOnInit() {
    if (this.clientId) {
      this.logger.debug("Edit Client ID: " + this.clientId);
      this.apiService.getClientById(this.clientId).subscribe(data => {
        this.logger.debug("Edit Client - Client by ID: " + JSON.stringify(data));
        this.dialogHeader = "Edit Client";
        this.client = data.data;
        this.clientForm = this.formBuilder.group({
          company_name: new FormControl(this.client.company_name, Validators.required),
          belongs_to: new FormControl(this.client.belongs_to, Validators.required),
          active_status: new FormControl(this.client.active_status, Validators.required),
          joined_date: new FormControl(this.client.joined_date, Validators.required),
          leaving_date: new FormControl(this.client.leaving_date)
        });
      }, error => {
        this.synergeService.handleErrorOnAPI(error);
      });
    } else {
      this.logger.debug("Create Client");
      this.dialogHeader = "Add Client";
      this.clientForm = this.formBuilder.group({
        company_name: new FormControl('', Validators.required),
        belongs_to: new FormControl('', Validators.required),
        active_status: new FormControl('', Validators.required),
        joined_date: new FormControl('', Validators.required),
        leaving_date: new FormControl('')
      });
    }
  }

  onSubmit() {
    this.submitted = true;
    if (this.clientForm.valid) {
      this.buttonText = "Loading...";
      this.client.company_name = this.clientForm.value.company_name;
      this.client.belongs_to = this.clientForm.value.belongs_to;
      this.client.active_status = this.clientForm.value.active_status;
      this.client.joined_date = this.clientForm.value.joined_date;
      this.client.leaving_date = this.clientForm.value.leaving_date;
      if (this.client.active_status === "Active")
        this.client.leaving_date = null;
      if (this.clientId) {
        this.apiService.updateClientById(this.client).subscribe((res: ResponseFormat) => {
          this.logger.debug("Client Update: " + JSON.stringify(res));
          if (res.success) {
            this.buttonText = "Success";
            setTimeout(() => {
              this.modalController.dismiss("update");
            }, 1000);
          } else {
            this.buttonText = "Confirm";
            this.synergeService.handleError(res.message);
          }
        }, error => {
          this.buttonText = "Confirm";
          this.synergeService.handleErrorOnAPI(error);
        });
      } else {
        this.apiService.createClient(this.client).subscribe((res: ResponseFormat) => {
          this.logger.debug("Client Create: " + JSON.stringify(res));
          if (res.success) {
            this.buttonText = "Success";
            setTimeout(() => {
              this.modalController.dismiss("update");
            }, 1000);
          } else {
            this.buttonText = "Confirm";
            this.synergeService.handleError(res.message);
          }
        }, error => {
          this.buttonText = "Confirm";
          this.synergeService.handleErrorOnAPI(error);
        });
      }
    }
  }

  public hasError = (controlName: string, errorName: string) => {
    return this.clientForm.controls[controlName].hasError(errorName);
  }

  dismiss() {
    this.modalController.dismiss();
  }

}
