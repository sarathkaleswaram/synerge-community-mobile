import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ModalController } from '@ionic/angular';
import { AppConstants } from 'src/app/domains/app-constants';
import { Clients } from 'src/app/domains/models/clients';
import { User } from 'src/app/domains/models/user';
import { ResponseFormat } from 'src/app/domains/response-format';
import { ApiService } from 'src/app/services/api.service';
import { LoggerService } from 'src/app/services/logger.service';
import { SynergeService } from 'src/app/services/synerge.service';
import { ValidatorService } from 'src/app/services/validator.service';
import { ChangePasswordComponent } from '../change-password/change-password.component';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.scss'],
})
export class EditProfileComponent implements OnInit {
  @Input() id: string;
  submitted = false;

  editProfileForm: FormGroup;
  user: User;
  clients: Clients[] = [];
  companyNames = [];

  imagePath;
  imgURL: any;

  buttonText = "Save";
  message: string;

  showChangePassword = true;

  constructor(
    private formBuilder: FormBuilder,
    private modalController: ModalController,
    public apiService: ApiService,
    public synergeService: SynergeService,
    private logger: LoggerService
  ) { }

  ngOnInit() {
    if (this.id) {
      if (this.synergeService.isAdmin) {
        this.apiService.getAllActiveClients().subscribe((res: ResponseFormat) => {
          this.logger.debug("Got clients count: " + res.data.length);
          this.clients = res.data;
          this.getUserData(this.id);
        }, error => {
          this.synergeService.handleErrorOnAPI(error);
        });
      } else {
        this.getUserData(this.id);
      }
    } else {
      this.synergeService.handleError('Invalid params. `user_id` is empty');
    }
  }

  getUserData(userId) {
    this.apiService.getUserById(userId).subscribe(data => {
      this.logger.debug("Edit Profile - User by ID: " + JSON.stringify(data));
      this.user = data.data;
      this.onBelongsToChange(this.user.belongs_to);
      if (this.user._id !== localStorage.getItem(AppConstants.USER_ID)) {
        this.showChangePassword = false;
      }
      this.editProfileForm = this.formBuilder.group({
        first_name: new FormControl(this.user.first_name, Validators.required),
        last_name: new FormControl(this.user.last_name, Validators.required),
        user_name: new FormControl(this.user.user_name, Validators.required),
        email: new FormControl(this.user.email, [Validators.required, Validators.email]),
        phone: new FormControl(this.user.phone, [Validators.required, ValidatorService.phoneMinLength]),
        role: new FormControl(this.user.role, Validators.required),
        belongs_to: new FormControl(this.user.belongs_to),
        company_name: new FormControl(this.user.company_name),
        address: new FormControl(this.user.address),
        city: new FormControl(this.user.city),
        country: new FormControl(this.user.country),
        postal_code: new FormControl(this.user.postal_code),
        designation: new FormControl(this.user.designation),
        bio: new FormControl(this.user.bio)
      });
    }, error => {
      this.synergeService.handleErrorOnAPI(error);
    });
  }

  handleBelongsToChangeEvent(event) {
    this.onBelongsToChange(event.detail.value);
  }

  onBelongsToChange(belongsTo) {
    this.logger.debug("onBelongsToChange: " + belongsTo);
    if (belongsTo === 'SYNERGE_I') {
      this.companyNames = this.clients.filter(x => x.belongs_to === 'SYNERGE_I');
    } else {
      this.companyNames = this.clients.filter(x => x.belongs_to === 'SYNERGE_II');
    }
  }

  onSubmit() {
    this.submitted = true;
    if (this.editProfileForm.valid) {
      if (this.imagePath) {
        this.buttonText = "Loading...";
        let formData: FormData = new FormData();
        if (this.synergeService.isMobile) {
          let file = this.synergeService.dataURItoBlob(this.imgURL);
          formData.append('uploadImage', file, `carousel-${(new Date().getTime()).toString(16)}.png`);
        } else {
          formData.append('uploadImage', this.imagePath);
        }
        this.apiService.uploadImage(formData).subscribe((res: ResponseFormat) => {
          if (res.success) {
            this.user.profile_pic_path = '/uploads/' + res.data;
            this.updateUser();
          }
        }, error => {
          this.buttonText = "Save";
          this.synergeService.handleErrorOnAPI(error);
        });
      } else {
        this.updateUser();
      }
    }
  }

  updateUser() {
    this.user.first_name = this.editProfileForm.value.first_name;
    this.user.last_name = this.editProfileForm.value.last_name;
    this.user.user_name = this.editProfileForm.value.user_name;
    this.user.email = this.editProfileForm.value.email;
    this.user.phone = this.editProfileForm.value.phone;
    this.user.role = this.editProfileForm.value.role;
    this.user.belongs_to = this.editProfileForm.value.belongs_to;
    this.user.company_name = this.editProfileForm.value.company_name;
    this.user.address = this.editProfileForm.value.address;
    this.user.city = this.editProfileForm.value.city;
    this.user.country = this.editProfileForm.value.country;
    this.user.postal_code = this.editProfileForm.value.postal_code;

    this.buttonText = "Loading...";
    this.apiService.updateUser(this.user).subscribe((res: ResponseFormat) => {
      this.logger.debug("Update Profile - User by ID: " + JSON.stringify(res));
      if (res.success) {
        this.buttonText = "Success";
        setTimeout(() => {
          this.modalController.dismiss("update");
        }, 1000);
      } else {
        this.buttonText = "Save";
        this.synergeService.handleError(res.message);
      }
    }, error => {
      this.buttonText = "Save";
      this.synergeService.handleErrorOnAPI(error);
    });
  }

  async onChangePassword() {
    const modal = await this.modalController.create({
      component: ChangePasswordComponent,
      componentProps: { user: this.user }
    });
    await modal.present();

    const { data } = await modal.onWillDismiss();
    if (data) {
      this.user = data;
    }
  }

  public hasError = (controlName: string, errorName: string) => {
    return this.editProfileForm.controls[controlName].hasError(errorName);
  }

  dismiss() {
    this.modalController.dismiss();
  }

}
