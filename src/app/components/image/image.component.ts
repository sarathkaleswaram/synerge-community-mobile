import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
// import { Camera, CameraOptions, PictureSourceType } from '@ionic-native/camera/ngx';
// import { File as IonicFile, FileEntry } from '@ionic-native/file/ngx';
import { ActionSheetController, ModalController, Platform } from '@ionic/angular';
import { ApiService } from 'src/app/services/api.service';
import { LoggerService } from 'src/app/services/logger.service';
import { SynergeService } from 'src/app/services/synerge.service';
import { ImageCropperComponentModal } from '../image-cropper-modal/image-cropper-modal.component';
// import { Crop } from '@ionic-native/crop/ngx';

@Component({
  selector: 'app-image',
  templateUrl: './image.component.html',
  styleUrls: ['./image.component.scss'],
})
export class ImageComponent implements OnInit {
  @Input() onButton: boolean;
  @Input() buttonText: string;
  @Input() aspectRatio: any;
  @Output() onUploadEvent: EventEmitter<any> = new EventEmitter();

  // options: CameraOptions = {
  //   quality: 25,
  //   saveToPhotoAlbum: false,
  //   destinationType: this.camera.DestinationType.FILE_URI,
  //   encodingType: this.camera.EncodingType.JPEG,
  //   mediaType: this.camera.MediaType.PICTURE
  // };
  img;
  imgURL;

  constructor(
    private actionSheetController: ActionSheetController,
    private platform: Platform,
    // private camera: Camera,
    // private file: IonicFile,
    // private crop: Crop,
    private modalController: ModalController,
    public apiService: ApiService,
    public synergeService: SynergeService,
    private logger: LoggerService
  ) { }

  ngOnInit() { }

  // async onChangePhoto() {
  //   const actionSheet = await this.actionSheetController.create({
  //     cssClass: 'brand-action-sheet',
  //     buttons: [{
  //       text: 'Load from Library',
  //       handler: () => {
  //         this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
  //       }
  //     },
  //     {
  //       text: 'Use Camera',
  //       handler: () => {
  //         this.takePicture(this.camera.PictureSourceType.CAMERA);
  //       }
  //     },
  //     {
  //       text: 'Cancel',
  //       role: 'cancel'
  //     }]
  //   });
  //   await actionSheet.present();
  // }

  // takePicture(sourceType: PictureSourceType) {
  //   if (this.synergeService.isMobile) {
  //     const options: CameraOptions = {
  //       quality: 50,
  //       sourceType: sourceType,
  //       saveToPhotoAlbum: false,
  //       correctOrientation: true
  //     };
  //     this.camera.getPicture(options).then(fileUri => {
  //       if (this.platform.is('ios')) {
  //         return fileUri;
  //       } else if (this.platform.is('android')) {
  //         return this.crop.crop(fileUri, { quality: 100, targetWidth: -1, targetHeight: -1 });
  //       }
  //     }, err => {
  //       this.logger.error(err);
  //     }).then((path) => {
  //       console.log('Cropped Image Path!: ' + path);
  //       this.img = {
  //         imgPath: (<any>window).Ionic.WebView.convertFileSrc(path)
  //       };
  //       this.file.resolveLocalFilesystemUrl(path).then((entry: FileEntry) => {
  //         entry.file(file => {
  //           this.readFile(file);
  //         });
  //       });
  //       return path;
  //     });
  //   } else {
  //     this.synergeService.displayAlert('Only works in mobile devices.');
  //   }
  // }

  async preview(event) {
    if (event.target.files.length === 0)
      return;

    let mimeType = event.target.files[0].type;
    if (mimeType.match(/image\/*/) == null) {
      this.synergeService.displayAlert("Only images are supported.");
      return;
    }

    const modal = await this.modalController.create({
      component: ImageCropperComponentModal,
      componentProps: {
        imageEvent: event,
        aspectRatio: this.aspectRatio
      }
    });
    await modal.present();

    const { data } = await modal.onWillDismiss();
    if (data) {
      this.onUploadEvent.emit([data.croppedImage, data.croppedFile]);
    }
  }

  async readFile(file: any) {
    // TODO add crop for mobile upload
    const reader = new FileReader();
    reader.onloadend = async () => {
      const imgBlob = new Blob([reader.result], {
        type: file.type
      });
      this.onUploadEvent.emit([this.img.imgPath, imgBlob, file.name]);
    };
    reader.readAsArrayBuffer(file);
  }

}
