import { Component, Input, OnInit } from '@angular/core';
import { AlertController, ModalController, PopoverController } from '@ionic/angular';
import { AppConstants } from 'src/app/domains/app-constants';
import { Comments, Likes, NewsFeed } from 'src/app/domains/models/newsFeed';
import { ResponseFormat } from 'src/app/domains/response-format';
import { ApiService } from 'src/app/services/api.service';
import { LoggerService } from 'src/app/services/logger.service';
import { SynergeService } from 'src/app/services/synerge.service';
import { ActionPopoverPage } from '../action-popover/action-popover';
import { NewsFeedModalComponent } from '../news-feed-modal/news-feed-modal.component';

@Component({
  selector: 'app-newsfeed-details',
  templateUrl: './newsfeed-details.component.html',
  styleUrls: ['./newsfeed-details.component.scss'],
})
export class NewsfeedDetailsComponent implements OnInit {
  @Input() id;
  newsFeed: NewsFeed;
  doUpdate = false;

  constructor(
    private apiService: ApiService,
    public synergeService: SynergeService,
    private logger: LoggerService,
    private modalController: ModalController,
    public alertController: AlertController,
    public popoverCtrl: PopoverController
  ) { }

  ngOnInit() {
    this.getNewsFeed();
  }

  getNewsFeed() {
    this.apiService.getNewsFeedById(this.id).subscribe(data => {
      this.logger.debug("News Feed by ID: " + JSON.stringify(data));
      this.newsFeed = data.data;
    }, error => {
      this.synergeService.handleErrorOnAPI(error);
    });
  }

  getDidILike(newsFeed: NewsFeed) {
    return newsFeed.likes.find(x => x.by_user_id === localStorage.getItem(AppConstants.USER_ID));
  }

  onLike(newsFeed: NewsFeed) {
    if (this.getDidILike(newsFeed)) {
      newsFeed.likes.splice(newsFeed.likes.findIndex(x => x.by_user_id === localStorage.getItem(AppConstants.USER_ID)), 1);
    } else {
      let like: Likes = {
        by_user_id: localStorage.getItem(AppConstants.USER_ID),
        by_user_name: localStorage.getItem(AppConstants.USER_FULL_NAME),
        date: new Date()
      };
      newsFeed.likes.push(like);
    }
    this.apiService.updateNewsFeedById(newsFeed).subscribe((res: ResponseFormat) => {
      this.logger.debug("Update news feed result: " + JSON.stringify(res));
      if (res.success) {
        this.doUpdate = true;
        this.getNewsFeed();
      } else {
        this.synergeService.handleError(res.message);
      }
    }, error => {
      this.synergeService.handleErrorOnAPI(error);
    });
  }

  async onAddComment() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Enter Comment',
      inputs: [
        {
          name: 'comment',
          id: 'comment',
          type: 'textarea',
          placeholder: 'Enter Comment'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Ok',
          handler: (data) => {
            if (data.comment) {
              let comment: Comments = {
                comment: data.comment,
                by_user_id: localStorage.getItem(AppConstants.USER_ID),
                by_user_name: localStorage.getItem(AppConstants.USER_FULL_NAME),
                date: new Date()
              };
              this.newsFeed.comments.push(comment);
              this.apiService.updateNewsFeedById(this.newsFeed).subscribe((res: ResponseFormat) => {
                this.logger.debug("Update news feed result: " + JSON.stringify(res));
                if (res.success) {
                  this.doUpdate = true;
                  this.getNewsFeed();
                } else {
                  this.synergeService.handleError(res.message);
                }
              }, error => {
                this.synergeService.handleErrorOnAPI(error);
              });
            }
          }
        }
      ]
    });

    await alert.present();
  }

  async presentPopover(event: Event) {
    const popover = await this.popoverCtrl.create({
      component: ActionPopoverPage,
      event,
      componentProps: {
        showEdit: true,
        showDelete: true
      }
    });
    await popover.present();

    const { data } = await popover.onWillDismiss();
    if (data) {
      if (data.action === 'Edit') {
        this.onEdit();
      }
      if (data.action === 'Delete') {
        this.onDelete();
      }
    }
  }

  async onEdit() {
    const modal = await this.modalController.create({
      component: NewsFeedModalComponent,
      componentProps: { newsfeedId: this.id }
    });
    await modal.present();

    const { data } = await modal.onWillDismiss();
    if (data) {
      this.doUpdate = true;
      this.getNewsFeed();
    }
  }

  async onDelete() {
    const alert = await this.alertController.create({
      header: 'Confirm!',
      message: 'Do you want to delete News Feed?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
        }, {
          text: 'Yes',
          handler: () => {
            this.apiService.deleteNewsFeedById(this.id).subscribe(data => {
              this.logger.debug("News Feed deleted by ID: " + JSON.stringify(data));
              this.modalController.dismiss("update");
            }, error => {
              this.synergeService.handleErrorOnAPI(error);
            });
          }
        }
      ]
    });
    await alert.present();
  }

  dismiss() {
    if (this.doUpdate)
      this.modalController.dismiss("update");
    else
      this.modalController.dismiss();
  }

}
