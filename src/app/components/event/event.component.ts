import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ModalController } from '@ionic/angular';
import { Event } from 'src/app/domains/models/events';
import { ResponseFormat } from 'src/app/domains/response-format';
import { ApiService } from 'src/app/services/api.service';
import { LoggerService } from 'src/app/services/logger.service';
import { SynergeService } from 'src/app/services/synerge.service';

@Component({
  selector: 'app-event',
  templateUrl: './event.component.html',
  styleUrls: ['./event.component.scss'],
})
export class EventComponent implements OnInit {
  @Input() eventId: string;
  eventForm: FormGroup;
  event: Event = {
    date: undefined,
    belongs_to: undefined,
    type: undefined,
    title: undefined,
    description: undefined,
    pics: [],
    links: [],
    likes: [],
    comments: []
  };
  imagePath;
  imgURL: any;
  message: string;
  buttonText = "Save";
  dialogHeader: string;
  submitted = false;

  constructor(
    private formBuilder: FormBuilder,
    private modalController: ModalController,
    public apiService: ApiService,
    public synergeService: SynergeService,
    private logger: LoggerService
  ) { }

  ngOnInit() {
    if (this.eventId) {
      this.logger.debug("Edit Event ID: " + this.eventId);
      this.apiService.getEventById(this.eventId).subscribe(data => {
        this.logger.debug("Edit Event - Event by ID: " + JSON.stringify(data));
        this.dialogHeader = "Edit Event";
        this.event = data.data;
        this.imgURL = this.synergeService.basePath + this.event.pics[0];
        this.eventForm = this.formBuilder.group({
          type: new FormControl(this.event.type, Validators.required),
          belongs_to: new FormControl(this.event.belongs_to, Validators.required),
          title: new FormControl(this.event.title, Validators.required),
          description: new FormControl(this.event.description),
          date: new FormControl(this.event.date, Validators.required),
          links: new FormControl(this.event.links[0])
        });
      }, error => {
        this.synergeService.handleErrorOnAPI(error);
      });
    } else {
      this.logger.debug("Create Event");
      this.dialogHeader = "Add Event";
      this.eventForm = this.formBuilder.group({
        type: new FormControl('', Validators.required),
        belongs_to: new FormControl('', Validators.required),
        title: new FormControl('', Validators.required),
        description: new FormControl(''),
        date: new FormControl('', Validators.required),
        links: new FormControl(''),
      });
    }
  }

  onSave() {
    this.submitted = true;
    if (this.imagePath) {
      this.buttonText = "Loading...";
      let formData: FormData = new FormData();
      if (this.synergeService.isMobile) {
        let file = this.synergeService.dataURItoBlob(this.imgURL);
        formData.append('uploadImage', file, `carousel-${(new Date().getTime()).toString(16)}.png`);
      } else {
        formData.append('uploadImage', this.imagePath);
      }
      this.apiService.uploadImage(formData).subscribe((res: ResponseFormat) => {
        if (res.success) {
          this.event.pics[0] = '/uploads/' + res.data;
          this.saveEvent();
        }
      }, error => {
        this.buttonText = "Save";
        this.synergeService.handleErrorOnAPI(error);
      });
    } else {
      this.saveEvent();
    }
  }

  onUpload(croppedImage, croppedFile) {
    this.imgURL = croppedImage;
    this.imagePath = croppedFile;
  }

  saveEvent() {
    if (this.eventForm.valid) {
      this.buttonText = "Loading...";
      this.event.date = this.eventForm.value.date;
      this.event.belongs_to = this.eventForm.value.belongs_to;
      this.event.type = this.eventForm.value.type;
      this.event.title = this.eventForm.value.title;
      this.event.description = this.eventForm.value.description;
      this.event.links[0] = this.eventForm.value.links;
      if (this.eventId) {
        this.apiService.updateEventById(this.event).subscribe((res: ResponseFormat) => {
          this.logger.debug("Event Update: " + JSON.stringify(res));
          if (res.success) {
            this.buttonText = "Success";
            setTimeout(() => {
              this.modalController.dismiss("update");
            }, 1000);
          } else {
            this.buttonText = "Save";
            this.synergeService.handleError(res.message);
          }
        }, error => {
          this.buttonText = "Save";
          this.synergeService.handleErrorOnAPI(error);
        });
      } else {
        this.apiService.createEvent(this.event).subscribe((res: ResponseFormat) => {
          this.logger.debug("Event res: " + JSON.stringify(res));
          if (res.success) {
            this.buttonText = "Success";
            setTimeout(() => {
              this.modalController.dismiss("update");
            }, 1000);
          } else {
            this.buttonText = "Save";
            this.message = res.message;
          }
        }, error => {
          this.buttonText = "Save";
          this.synergeService.handleErrorOnAPI(error);
        });
      }
    }
  }

  public hasError = (controlName: string, errorName: string) => {
    return this.eventForm.controls[controlName].hasError(errorName);
  }

  dismiss() {
    this.modalController.dismiss();
  }

}
