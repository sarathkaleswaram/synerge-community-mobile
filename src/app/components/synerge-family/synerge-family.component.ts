import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ModalController } from '@ionic/angular';
import { CompanyHead, SynergeFamily } from 'src/app/domains/models/synergeFamily';
import { ResponseFormat } from 'src/app/domains/response-format';
import { ApiService } from 'src/app/services/api.service';
import { LoggerService } from 'src/app/services/logger.service';
import { SynergeService } from 'src/app/services/synerge.service';

@Component({
  selector: 'app-synerge-family',
  templateUrl: './synerge-family.component.html',
  styleUrls: ['./synerge-family.component.scss'],
})
export class SynergeFamilyComponent implements OnInit {
  @Input() synergeFamilyId: string;

  synergeFamily: SynergeFamily = {
    belongs_to: undefined,
    company_name: undefined,
    company_description: undefined,
    company_link: undefined,
    company_logo_path: undefined,
    company_heads: []
  };
  synergeFamilyForm: FormGroup;
  buttonText = "Save";
  imagePath;
  imgURL: any;
  message: string;
  companyHeads: CompanyHead[] = [{
    designation: undefined,
    name: undefined
  }];
  dialogHeader: string;
  submitted = false;

  constructor(
    private formBuilder: FormBuilder,
    private modalController: ModalController,
    public apiService: ApiService,
    public synergeService: SynergeService,
    private logger: LoggerService
  ) { }

  ngOnInit() {
    if (this.synergeFamilyId) {
      this.logger.debug("Edit SynergeFamily ID: " + this.synergeFamilyId);
      this.apiService.getSynergeFamilyById(this.synergeFamilyId).subscribe(data => {
        this.logger.debug("Edit Synerge Family by ID: " + JSON.stringify(data));
        this.dialogHeader = "Edit Synerge Family";
        this.synergeFamily = data.data;
        this.imgURL = this.synergeService.basePath + this.synergeFamily.company_logo_path;
        this.companyHeads = this.synergeFamily.company_heads;
        this.synergeFamilyForm = this.formBuilder.group({
          belongs_to: new FormControl(this.synergeFamily.belongs_to, Validators.required),
          company_name: new FormControl(this.synergeFamily.company_name, Validators.required),
          company_description: new FormControl(this.synergeFamily.company_description),
          company_link: new FormControl(this.synergeFamily.company_link),
        });
      }, error => {
        this.synergeService.handleErrorOnAPI(error);
      });
    } else {
      this.logger.debug("Create Synergefamily");
      this.dialogHeader = "Add Synerge Family";
      this.synergeFamilyForm = this.formBuilder.group({
        belongs_to: new FormControl('', Validators.required),
        company_name: new FormControl('', Validators.required),
        company_description: new FormControl(''),
        company_link: new FormControl('')
      });
    }
  }

  onUpload(croppedImage, croppedFile) {
    this.imgURL = croppedImage;
    this.imagePath = croppedFile;
  }

  onSave() {
    this.submitted = true;
    if (this.imagePath) {
      this.buttonText = "Loading...";
      let formData: FormData = new FormData();
      if (this.synergeService.isMobile) {
        let file = this.synergeService.dataURItoBlob(this.imgURL);
        formData.append('uploadImage', file, `carousel-${(new Date().getTime()).toString(16)}.png`);
      } else {
        formData.append('uploadImage', this.imagePath);
      }
      this.apiService.uploadImage(formData).subscribe((res: ResponseFormat) => {
        if (res.success) {
          this.synergeFamily.company_logo_path = '/uploads/' + res.data;
          this.saveSynergeFamily();
        }
      }, error => {
        this.buttonText = "Save";
        this.synergeService.handleErrorOnAPI(error);
      });
    } else {
      this.saveSynergeFamily();
    }
  }

  saveSynergeFamily() {
    if (this.synergeFamilyForm.valid) {
      this.buttonText = "Loading...";
      this.companyHeads = this.companyHeads.filter(x => x.designation && x.name);
      this.synergeFamily.belongs_to = this.synergeFamilyForm.value.belongs_to;
      this.synergeFamily.company_name = this.synergeFamilyForm.value.company_name;
      this.synergeFamily.company_description = this.synergeFamilyForm.value.company_description;
      this.synergeFamily.company_link = this.synergeFamilyForm.value.company_link;
      this.synergeFamily.company_heads = this.companyHeads;
      if (this.synergeFamilyId) {
        this.apiService.updateSynergeFamilyById(this.synergeFamily).subscribe((res: ResponseFormat) => {
          this.logger.debug("Update SynergeFamily res: " + JSON.stringify(res));
          if (res.success) {
            this.buttonText = "Success";
            setTimeout(() => {
              this.modalController.dismiss("udpate");
            }, 1000);
          } else {
            this.buttonText = "Save";
            this.message = res.message;
          }
        }, error => {
          this.buttonText = "Save";
          this.synergeService.handleErrorOnAPI(error);
        });
      } else {
        this.apiService.createSynergeFamily(this.synergeFamily).subscribe((res: ResponseFormat) => {
          this.logger.debug("Save SynergeFamily res: " + JSON.stringify(res));
          if (res.success) {
            this.buttonText = "Success";
            setTimeout(() => {
              this.modalController.dismiss("udpate");
            }, 1000);
          } else {
            this.buttonText = "Save";
            this.message = res.message;
          }
        }, error => {
          this.buttonText = "Save";
          this.synergeService.handleErrorOnAPI(error);
        });
      }
    }
  }

  addRow(i: number) {
    let companyHeads: CompanyHead = { designation: undefined, name: undefined };
    if (this.companyHeads.length <= i + 1) {
      this.companyHeads.splice(i + 1, 0, companyHeads);
    }
  }

  deleteRow($event, companyHeads: CompanyHead, i) {
    let index = this.companyHeads.indexOf(companyHeads);
    if ($event.which == 1) {
      this.companyHeads.splice(index, 1);
    }
  }

  public hasError = (controlName: string, errorName: string) => {
    return this.synergeFamilyForm.controls[controlName].hasError(errorName);
  }

  dismiss() {
    this.modalController.dismiss();
  }

}
