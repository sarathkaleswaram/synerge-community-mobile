import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Likes } from 'src/app/domains/models/events';
import { SynergeService } from 'src/app/services/synerge.service';

@Component({
  selector: 'app-likes-list',
  templateUrl: './likes-list.component.html',
  styleUrls: ['./likes-list.component.scss'],
})
export class LikesListComponent implements OnInit {
  @Input() likes: Likes[];

  constructor(
    public synergeService: SynergeService,
    private modalController: ModalController
  ) { }

  ngOnInit() { }

  dismiss() {
    this.modalController.dismiss();
  }

}
