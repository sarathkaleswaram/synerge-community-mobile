import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ApiService } from 'src/app/services/api.service';
import { SynergeService } from 'src/app/services/synerge.service';
import { LoggerService } from 'src/app/services/logger.service';
import { NewsFeed } from 'src/app/domains/models/newsFeed';
import { ResponseFormat } from 'src/app/domains/response-format';
import { AppConstants } from 'src/app/domains/app-constants';

@Component({
  selector: 'app-news-feed-modal',
  templateUrl: './news-feed-modal.component.html',
  styleUrls: ['./news-feed-modal.component.scss'],
})
export class NewsFeedModalComponent implements OnInit {
  @Input() newsfeedId: string;
  newsFeed: NewsFeed = {
    by_user_id: undefined,
    by_user_name: undefined,
    date: undefined,
    content: undefined,
    image_path: undefined,
    likes: [],
    comments: []
  };
  imgURL;
  imagePath;
  content: string = "";
  buttonText = "Save";

  constructor(
    private modalController: ModalController,
    public apiService: ApiService,
    public synergeService: SynergeService,
    private logger: LoggerService,
  ) { }

  ngOnInit() {
    if (this.newsfeedId) {
      this.logger.debug("Edit Newsfeed ID: " + this.newsfeedId);
      this.apiService.getNewsFeedById(this.newsfeedId).subscribe(data => {
        this.logger.debug("Edit Newsfeed ID: " + JSON.stringify(data));
        this.newsFeed = data.data;
        this.content = this.newsFeed.content;
        this.imgURL = this.synergeService.basePath + this.newsFeed.image_path;
      }, error => {
        this.synergeService.handleErrorOnAPI(error);
      });
    }
  }

  onUpload(croppedImage, croppedFile) {
    this.imgURL = croppedImage;
    this.imagePath = croppedFile;
  }

  onSave() {
    if (this.imagePath) {
      this.buttonText = "Loading...";
      let formData: FormData = new FormData();
      if (this.synergeService.isMobile) {
        let file = this.synergeService.dataURItoBlob(this.imgURL);
        formData.append('uploadImage', file, `carousel-${(new Date().getTime()).toString(16)}.png`);
      } else {
        formData.append('uploadImage', this.imagePath);
      }
      this.apiService.uploadImage(formData).subscribe((res: ResponseFormat) => {
        if (res.success) {
          this.newsFeed.image_path = '/uploads/' + res.data;
          this.saveContent();
        }
      }, error => {
        this.buttonText = "Save";
        this.synergeService.handleErrorOnAPI(error);
      });
    } else {
      this.saveContent();
    }
  }

  saveContent() {
    if (this.content.length > 0) {
      this.buttonText = "Loading...";
      if (this.newsfeedId) {
        this.newsFeed.content = this.content;
        this.apiService.updateNewsFeedById(this.newsFeed).subscribe((res: ResponseFormat) => {
          this.logger.debug("News Feed: " + JSON.stringify(res));
          if (res.success) {
            this.buttonText = "Success";
            setTimeout(() => {
              this.modalController.dismiss("Success");
            }, 1000);
          } else {
            this.buttonText = "Save";
            this.synergeService.displayAlert(res.message);
          }
        }, error => {
          this.buttonText = "Save";
          this.synergeService.handleErrorOnAPI(error);
        });
      } else {
        this.newsFeed.content = this.content;
        this.newsFeed.date = new Date();
        this.newsFeed.by_user_id = localStorage.getItem(AppConstants.USER_ID);
        this.newsFeed.by_user_name = localStorage.getItem(AppConstants.USER_FULL_NAME);
        this.apiService.saveNewsFeed(this.newsFeed).subscribe((res: ResponseFormat) => {
          this.logger.debug("News Feed: " + JSON.stringify(res));
          if (res.success) {
            this.buttonText = "Success";
            setTimeout(() => {
              this.modalController.dismiss("Success");
            }, 1000);
          } else {
            this.buttonText = "Save";
            this.synergeService.displayAlert(res.message);
          }
        }, error => {
          this.buttonText = "Save";
          this.synergeService.handleErrorOnAPI(error);
        });
      }
    } else {
      this.synergeService.displayAlert("Please enter content.");
    }
  }

  dismiss() {
    this.modalController.dismiss();
  }

}
