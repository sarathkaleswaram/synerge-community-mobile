import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ModalController } from '@ionic/angular';
import { Achiever } from 'src/app/domains/models/achievers';
import { Clients } from 'src/app/domains/models/clients';
import { ResponseFormat } from 'src/app/domains/response-format';
import { ApiService } from 'src/app/services/api.service';
import { LoggerService } from 'src/app/services/logger.service';
import { SynergeService } from 'src/app/services/synerge.service';

@Component({
  selector: 'app-achiever',
  templateUrl: './achiever.component.html',
  styleUrls: ['./achiever.component.scss'],
})
export class AchieverComponent implements OnInit {
  @Input() achieverId: string;

  achiever: Achiever = {
    belongs_to: undefined,
    company_name: undefined,
    achiever: undefined,
    achieved: undefined,
    pic: undefined,
    date: undefined
  };
  clients: Clients[] = [];
  companyNames = [];
  achieverForm: FormGroup;
  buttonText = "Save";
  imagePath;
  imgURL: any;
  message: string;
  dialogHeader: string;

  submitted = false;

  constructor(
    private formBuilder: FormBuilder,
    private modalController: ModalController,
    public apiService: ApiService,
    public synergeService: SynergeService,
    private logger: LoggerService
  ) { }

  ngOnInit() {
    this.apiService.getAllActiveClients().subscribe((res: ResponseFormat) => {
      this.logger.debug("Got clients count: " + res.data.length);
      this.clients = res.data;
    }, error => {
      this.synergeService.handleErrorOnAPI(error);
    });
    if (this.achieverId) {
      this.logger.debug("Edit Achiever ID: " + this.achieverId);
      this.apiService.getAchieverById(this.achieverId).subscribe(data => {
        this.dialogHeader = "Edit Achiever"
        this.logger.debug("Edit Achiever by ID: " + JSON.stringify(data));
        this.achiever = data.data;
        this.onBelongsToChange(this.achiever.belongs_to);
        this.imgURL = this.synergeService.basePath + this.achiever.pic;
        this.achieverForm = this.formBuilder.group({
          belongs_to: new FormControl(this.achiever.belongs_to, Validators.required),
          company_name: new FormControl(this.achiever.company_name, Validators.required),
          achiever: new FormControl(this.achiever.achiever, Validators.required),
          achieved: new FormControl(this.achiever.achieved, Validators.required),
          date: new FormControl(this.achiever.date, Validators.required),
        });
      }, error => {
        this.synergeService.handleErrorOnAPI(error);
      });
    } else {
      this.logger.debug("Create Achiever");
      this.dialogHeader = "Add Achiever"
      this.achieverForm = this.formBuilder.group({
        belongs_to: new FormControl('', Validators.required),
        company_name: new FormControl('', Validators.required),
        achiever: new FormControl('', Validators.required),
        achieved: new FormControl('', Validators.required),
        date: new FormControl('', Validators.required)
      });
    }
  }

  handleBelongsToChangeEvent(event) {
    this.onBelongsToChange(event.detail.value);
  }

  onBelongsToChange(belongsTo) {
    this.logger.debug("onBelongsToChange: " + belongsTo);
    if (belongsTo === 'SYNERGE_I') {
      this.companyNames = this.clients.filter(x => x.belongs_to === 'SYNERGE_I');
    } else {
      this.companyNames = this.clients.filter(x => x.belongs_to === 'SYNERGE_II');
    }
  }

  onUpload(croppedImage, croppedFile) {
    this.imgURL = croppedImage;
    this.imagePath = croppedFile;
  }

  onSave() {
    this.submitted = true;
    if (this.imagePath) {
      this.buttonText = "Loading...";
      let formData: FormData = new FormData();
      if (this.synergeService.isMobile) {
        let file = this.synergeService.dataURItoBlob(this.imgURL);
        formData.append('uploadImage', file, `carousel-${(new Date().getTime()).toString(16)}.png`);
      } else {
        formData.append('uploadImage', this.imagePath);
      }
      this.apiService.uploadImage(formData).subscribe((res: ResponseFormat) => {
        if (res.success) {
          this.achiever.pic = '/uploads/' + res.data;
          this.saveAchiever();
        }
      }, error => {
        this.buttonText = "Save";
        this.synergeService.handleErrorOnAPI(error);
      });
    } else {
      this.saveAchiever();
    }
  }

  saveAchiever() {
    if (this.achieverForm.valid) {
      this.buttonText = "Loading...";
      this.achiever.belongs_to = this.achieverForm.value.belongs_to;
      this.achiever.company_name = this.achieverForm.value.company_name;
      this.achiever.achieved = this.achieverForm.value.achieved;
      this.achiever.achiever = this.achieverForm.value.achiever;
      this.achiever.date = this.achieverForm.value.date;
      if (this.achieverId) {
        this.apiService.updateAchieverById(this.achiever).subscribe((res: ResponseFormat) => {
          this.logger.debug("Update Achiever res: " + JSON.stringify(res));
          if (res.success) {
            this.buttonText = "Success";
            setTimeout(() => {
              this.modalController.dismiss("update");
            }, 1000);
          } else {
            this.buttonText = "Save";
            this.message = res.message;
          }
        }, error => {
          this.buttonText = "Save";
          this.synergeService.handleErrorOnAPI(error);
        });
      } else {
        this.apiService.createAchiever(this.achiever).subscribe((res: ResponseFormat) => {
          this.logger.debug("Save Achiever res: " + JSON.stringify(res));
          if (res.success) {
            this.buttonText = "Success";
            setTimeout(() => {
              this.modalController.dismiss("update");
            }, 1000);
          } else {
            this.buttonText = "Save";
            this.message = res.message;
          }
        }, error => {
          this.buttonText = "Save";
          this.synergeService.handleErrorOnAPI(error);
        });
      }
    }
  }

  public hasError = (controlName: string, errorName: string) => {
    return this.achieverForm.controls[controlName].hasError(errorName);
  }

  dismiss() {
    this.modalController.dismiss();
  }

}
