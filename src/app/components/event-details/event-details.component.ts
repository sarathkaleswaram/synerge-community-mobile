import { Component, Input, OnInit } from '@angular/core';
import { AlertController, ModalController, PopoverController } from '@ionic/angular';
import { AppConstants } from 'src/app/domains/app-constants';
import { Comments, Event, Likes } from 'src/app/domains/models/events';
import { ResponseFormat } from 'src/app/domains/response-format';
import { ApiService } from 'src/app/services/api.service';
import { LoggerService } from 'src/app/services/logger.service';
import { SynergeService } from 'src/app/services/synerge.service';
import { ActionPopoverPage } from '../action-popover/action-popover';
import { EventComponent } from '../event/event.component';

@Component({
  selector: 'app-event-details',
  templateUrl: './event-details.component.html',
  styleUrls: ['./event-details.component.scss'],
})
export class EventDetailsComponent implements OnInit {
  @Input() eventId;
  event: Event;
  doUpdate = false;

  constructor(
    private apiService: ApiService,
    public synergeService: SynergeService,
    private logger: LoggerService,
    private modalController: ModalController,
    public alertController: AlertController,
    public popoverCtrl: PopoverController
  ) { }

  ngOnInit() {
    this.getEvent();
  }

  getEvent() {
    this.apiService.getEventById(this.eventId).subscribe(data => {
      this.logger.debug("Event by ID: " + JSON.stringify(data));
      this.event = data.data;
    }, error => {
      this.synergeService.handleErrorOnAPI(error);
    });
  }

  getDidILike(event: Event) {
    return event.likes.find(x => x.by_user_id === localStorage.getItem(AppConstants.USER_ID));
  }  

  async onAddComment() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Enter Comment',
      inputs: [
        {
          name: 'comment',
          id: 'comment',
          type: 'textarea',
          placeholder: 'Enter Comment'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Ok',
          handler: (data) => {
            if (data.comment) {
              let comment: Comments = {
                comment: data.comment,
                by_user_id: localStorage.getItem(AppConstants.USER_ID),
                by_user_name: localStorage.getItem(AppConstants.USER_FULL_NAME),
                date: new Date()
              };
              this.event.comments.push(comment);
              this.apiService.updateEventById(this.event).subscribe((res: ResponseFormat) => {
                this.logger.debug("Update event result: " + JSON.stringify(res));
                if (res.success) {
                  this.doUpdate = true;
                  this.getEvent();
                } else {
                  this.synergeService.handleError(res.message);
                }
              }, error => {
                this.synergeService.handleErrorOnAPI(error);
              });
            }
          }
        }
      ]
    });

    await alert.present();
  }

  async presentPopover(event) {
    const popover = await this.popoverCtrl.create({
      component: ActionPopoverPage,
      event,
      componentProps: {
        showEdit: true,
        showDelete: true
      }
    });
    await popover.present();

    const { data } = await popover.onWillDismiss();
    this.logger.debug("Popover data: " + JSON.stringify(data));
    if (data) {
      if (data.action === 'Edit') {
        this.editEvent(this.event);
      }
      if (data.action === 'Delete') {
        this.deleteEvent(this.event);
      }
    }
  }

  async editEvent(event: Event) {
    const modal = await this.modalController.create({
      component: EventComponent,
      componentProps: { eventId: event._id }
    });
    await modal.present();

    const { data } = await modal.onWillDismiss();
    if (data) {
      this.doUpdate = true;
      this.getEvent();
    }
  }

  async deleteEvent(event: Event) {
    this.logger.debug("On Delete: " + JSON.stringify(event));
    const alert = await this.alertController.create({
      header: 'Confirm!',
      message: `Do you want to delete ${event.title}?`,
      buttons: [
        {
          text: 'No',
          role: 'cancel',
        }, {
          text: 'Yes',
          handler: () => {
            this.apiService.deleteEventById(event._id).subscribe((res: ResponseFormat) => {
              this.logger.debug("Event Deleted by ID: " + JSON.stringify(res));
              if (res.success) {
                this.modalController.dismiss("update");
              } else {
                this.synergeService.handleError(res.message);
              }
            }, error => {
              this.synergeService.handleErrorOnAPI(error);
            });
          }
        }
      ]
    });
    await alert.present();
  }

  onLike(event: Event) {
    if (this.getDidILike(event)) {
      event.likes.splice(event.likes.findIndex(x => x.by_user_id === localStorage.getItem(AppConstants.USER_ID)), 1);
    } else {
      let like: Likes = {
        by_user_id: localStorage.getItem(AppConstants.USER_ID),
        by_user_name: localStorage.getItem(AppConstants.USER_FULL_NAME),
        date: new Date()
      };
      event.likes.push(like);
    }
    this.apiService.updateEventById(event).subscribe((res: ResponseFormat) => {
      this.logger.debug("Update events result: " + JSON.stringify(res));
      if (res.success) {
        this.doUpdate = true;
        this.getEvent();
      } else {
        this.synergeService.handleError(res.message);
      }
    }, error => {
      this.synergeService.handleErrorOnAPI(error);
    });
  }

  dismiss() {
    if (this.doUpdate)
      this.modalController.dismiss("update");
    else
      this.modalController.dismiss();
  }

}
