import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { BelongsToEnum, Clients } from 'src/app/domains/models/clients';
import { ResponseFormat } from 'src/app/domains/response-format';
import { ApiService } from 'src/app/services/api.service';
import { LoggerService } from 'src/app/services/logger.service';
import { SynergeService } from 'src/app/services/synerge.service';

@Component({
  selector: 'app-select-client-company',
  templateUrl: './select-client-company.component.html',
  styleUrls: ['./select-client-company.component.scss'],
})
export class SelectClientCompanyComponent implements OnInit {
  clients: Clients[] = [];
  companyNames = [];
  belongs_to: BelongsToEnum;
  company_name = "";

  constructor(
    private apiService: ApiService,
    public synergeService: SynergeService,
    private modalController: ModalController,
    private logger: LoggerService
    ) { }

  ngOnInit() {
    this.apiService.getAllActiveClients().subscribe((res: ResponseFormat) => {
      this.logger.debug("Got clients count: " + res.data.length);
      this.clients = res.data;
    }, error => {
      this.synergeService.handleErrorOnAPI(error);
    });
  }

  onBelongsToChange(belongsTo) {
    this.logger.debug("onBelongsToChange: " + belongsTo);
    this.company_name = "";
    if (belongsTo === 'SYNERGE_I') {
      this.companyNames = this.clients.filter(x => x.belongs_to === 'SYNERGE_I');
    } else {
      this.companyNames = this.clients.filter(x => x.belongs_to === 'SYNERGE_II');
    }
  }

  onSelect() {
    if (this.belongs_to === undefined || this.company_name === "") {
      this.synergeService.displayAlert("Please enter all fields.");
      return;
    }
    this.modalController.dismiss({ belongs_to: this.belongs_to, company_name: this.company_name });
  }

  dismiss() {
    this.modalController.dismiss();
  }

}
