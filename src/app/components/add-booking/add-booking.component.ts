import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { AppConstants } from 'src/app/domains/app-constants';
import { Booking } from 'src/app/domains/models/bookings';
import { ResponseFormat } from 'src/app/domains/response-format';
import { ApiService } from 'src/app/services/api.service';
import { LoggerService } from 'src/app/services/logger.service';
import { SynergeService } from 'src/app/services/synerge.service';
import { BookingTimeComponent } from '../booking-time/booking-time.component';

@Component({
  selector: 'app-add-booking',
  templateUrl: './add-booking.component.html',
  styleUrls: ['./add-booking.component.scss'],
})
export class AddBookingComponent implements OnInit {
  @Input() belongs_to: string;
  @Input() company_name: string;

  booking: Booking;

  selectedType: string;
  selectedDate: Date;
  selectedTime: string;

  bookingNote: string;

  discussionRoomString = "";

  minDate = new Date().toISOString();

  buttonText = "Submit";
  message: string = "";
  showSuccessMessage: boolean = false;

  constructor(
    private apiService: ApiService,
    public synergeService: SynergeService,
    private modalController: ModalController,
    private logger: LoggerService
  ) { }

  ngOnInit() {
    if (this.belongs_to === 'SYNERGE_I') {
      this.discussionRoomString = 'Discussion room / Training room';
    } else {
      this.discussionRoomString = 'Discussion room';
    }
  }

  onDateChange(date) {
    this.displayTimes();
  }

  dismiss() {
    if (this.showSuccessMessage)
      this.modalController.dismiss("update");
    else
      this.modalController.dismiss();
  }

  async displayTimes() {
    const modal = await this.modalController.create({
      component: BookingTimeComponent,
      componentProps: {
        selectedDate: this.selectedDate,
        belongs_to: this.belongs_to,
        booking_type: this.selectedType,
      }
    });
    await modal.present();

    const { data } = await modal.onWillDismiss();
    if (data) {
      this.selectedTime = data.selectedTime;
    }
  }

  onSubmit() {
    this.message = "";
    if (this.selectedType && this.selectedDate && this.selectedTime) {
      let bookingDate = new Date(this.selectedDate);
      this.buttonText = "Loading...";
      this.booking = {
        by_user_id: localStorage.getItem(AppConstants.USER_ID),
        by_user_name: localStorage.getItem(AppConstants.USER_FULL_NAME),
        belongs_to: this.belongs_to,
        company_name: this.company_name,
        booking_created_date: new Date(),
        booking_type: this.selectedType,
        booking_date: new Date(bookingDate.getFullYear(), bookingDate.getMonth(), bookingDate.getDate()),
        booking_time: this.selectedTime,
        status: "Request Pending",
        booking_note: this.bookingNote
      };
      this.apiService.addBooking(this.booking).subscribe((res: ResponseFormat) => {
        this.logger.debug("Booking Saved data: " + JSON.stringify(res));
        if (res.success) {
          this.buttonText = "Success";
          this.showSuccessMessage = true;
        } else {
          this.buttonText = "Submit";
          this.synergeService.handleError(res.message);
        }
      }, error => {
        this.buttonText = "Submit";
        this.synergeService.handleErrorOnAPI(error);
      });
    } else {
      this.message = "Please select a Booking with Date and Time.";
    }
  }

}
