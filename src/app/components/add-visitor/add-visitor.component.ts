import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ModalController } from '@ionic/angular';
import { Clients } from 'src/app/domains/models/clients';
import { ClientVisitors } from 'src/app/domains/models/clientVisitors';
import { User } from 'src/app/domains/models/user';
import { ResponseFormat } from 'src/app/domains/response-format';
import { ApiService } from 'src/app/services/api.service';
import { LoggerService } from 'src/app/services/logger.service';
import { SynergeService } from 'src/app/services/synerge.service';

@Component({
  selector: 'app-add-visitor',
  templateUrl: './add-visitor.component.html',
  styleUrls: ['./add-visitor.component.scss'],
})
export class AddVisitorComponent implements OnInit {
  clients: Clients[] = [];
  users: User[] = [];
  companyNames = [];
  companyUsers: User[] = [];
  clientVisitorsForm: FormGroup;
  buttonText = "Confirm";
  submitted = false;

  constructor(
    private formBuilder: FormBuilder,
    private modalController: ModalController,
    public apiService: ApiService,
    public synergeService: SynergeService,
    private logger: LoggerService
  ) { }

  ngOnInit() {
    this.apiService.getAllActiveClients().subscribe((res: ResponseFormat) => {
      this.logger.debug("Got clients count: " + res.data.length);
      this.clients = res.data;
    }, error => {
      this.synergeService.handleErrorOnAPI(error);
    });
    this.apiService.getAllUsers().subscribe((res: ResponseFormat) => {
      this.logger.debug("Got users count: " + res.data.length);
      this.users = res.data;
    }, error => {
      this.synergeService.handleErrorOnAPI(error);
    });
    this.clientVisitorsForm = this.formBuilder.group({
      visitor_name: new FormControl('', Validators.required),
      visitor_phone: new FormControl('', Validators.required),
      company_belongs_to: new FormControl('', Validators.required),
      visiting_company: new FormControl('', Validators.required),
      purpose_of_visit: new FormControl('', Validators.required),
      whom_to_contact_id: new FormControl('', Validators.required),
    });
  }

  onBelongsToChange(event) {
    let belongsTo = event.detail.value;
    this.logger.debug("onBelongsToChange: " + belongsTo);
    this.companyUsers = [];
    this.companyNames = [];
    if (belongsTo === 'SYNERGE_I') {
      this.companyNames = this.clients.filter(x => x.belongs_to === 'SYNERGE_I');
    } else {
      this.companyNames = this.clients.filter(x => x.belongs_to === 'SYNERGE_II');
    }
  }
  
  onCompanyChange(event) {
    let companyName = event.detail.value;
    this.logger.debug("onCompanyChange: " + companyName);
    this.companyUsers = this.users.filter(x => x.company_name === companyName);
  }

  onSubmit() {
    this.submitted = true;
    if (this.clientVisitorsForm.valid) {
      this.buttonText = "Loading...";
      let selectedUser = this.companyUsers.find(x => x._id === this.clientVisitorsForm.get('whom_to_contact_id').value);
      let clientVisitors: ClientVisitors = {
        visitor_name: this.clientVisitorsForm.get('visitor_name').value,
        visitor_phone: this.clientVisitorsForm.get('visitor_phone').value,
        company_belongs_to: this.clientVisitorsForm.get('company_belongs_to').value,
        visiting_company: this.clientVisitorsForm.get('visiting_company').value,
        whom_to_contact_id: this.clientVisitorsForm.get('whom_to_contact_id').value,
        whom_to_contact: selectedUser ? `${selectedUser.first_name} ${selectedUser.last_name}` : '',
        purpose_of_visit: this.clientVisitorsForm.get('purpose_of_visit').value,
        visiting_time: new Date(),
      };
      this.apiService.createClientVisitor(clientVisitors).subscribe((res: ResponseFormat) => {
        this.logger.debug("ClientVisitor Create: " + JSON.stringify(res));
        if (res.success) {
          this.buttonText = "Success";
          setTimeout(() => {
            this.modalController.dismiss("update");
          }, 1000);
        } else {
          this.buttonText = "Confirm";
          this.synergeService.handleError(res.message);
        }
      }, error => {
        this.buttonText = "Confirm";
        this.synergeService.handleErrorOnAPI(error);
      });
    }
  }

  public hasError = (controlName: string, errorName: string) => {
    return this.clientVisitorsForm.controls[controlName].hasError(errorName);
  }

  dismiss() {
    this.modalController.dismiss();
  }

}
