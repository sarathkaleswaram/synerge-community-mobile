import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { User } from 'src/app/domains/models/user';
import { ApiService } from 'src/app/services/api.service';
import { LoggerService } from 'src/app/services/logger.service';
import { SynergeService } from 'src/app/services/synerge.service';

@Component({
  selector: 'app-user-info',
  templateUrl: './user-info.component.html',
  styleUrls: ['./user-info.component.scss'],
})
export class UserInfoComponent implements OnInit {
  @Input() userId: string;
  user: User;

  constructor(private apiService: ApiService,
    private modalController: ModalController,
    public synergeService: SynergeService,
    private logger: LoggerService
  ) { }

  ngOnInit() {
    this.apiService.getUserById(this.userId).subscribe(data => {
      this.logger.debug("User By ID - User by ID: " + JSON.stringify(data));
      this.user = data.data;
    }, error => {
      this.synergeService.handleErrorOnAPI(error);
    });
  }

  dismiss() {
    this.modalController.dismiss();
  }

}
