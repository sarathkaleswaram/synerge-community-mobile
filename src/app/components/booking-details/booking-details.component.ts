import { Component, Input, OnInit } from '@angular/core';
import { AlertController, ModalController } from '@ionic/angular';
import { Booking, StatusEnum } from 'src/app/domains/models/bookings';
import { User } from 'src/app/domains/models/user';
import { ResponseFormat } from 'src/app/domains/response-format';
import { ApiService } from 'src/app/services/api.service';
import { LoggerService } from 'src/app/services/logger.service';
import { SynergeService } from 'src/app/services/synerge.service';

@Component({
  selector: 'app-booking-details',
  templateUrl: './booking-details.component.html',
  styleUrls: ['./booking-details.component.scss'],
})
export class BookingDetailsComponent implements OnInit {
  @Input() id;
  user: User;
  booking: Booking;
  status: StatusEnum;
  saveButtonText = "Save";
  deleteButtonText = "Delete";

  constructor(
    private apiService: ApiService,
    public synergeService: SynergeService,
    private modalController: ModalController,
    public alertController: AlertController,
    private logger: LoggerService
  ) { }

  ngOnInit() {
    if (this.id) {
      this.apiService.getBookingById(this.id).subscribe((res: ResponseFormat) => {
        this.logger.debug("Booked data: " + JSON.stringify(res));
        if (res.success) {
          this.booking = res.data;
          this.status = this.booking.status;
          this.apiService.getUserById(this.booking.by_user_id).subscribe((res: ResponseFormat) => {
            this.logger.debug("Booked User: " + JSON.stringify(res));
            if (res.success) {
              this.user = res.data;
            } else {
              this.synergeService.handleError(res.message);
            }
          }, error => {
            this.synergeService.handleErrorOnAPI(error);
          });
        } else {
          this.synergeService.handleError(res.message);
        }
      }, error => {
        this.synergeService.handleErrorOnAPI(error);
      });
    } else {
      this.synergeService.displayAlert('Empty id');
    }
  }

  onUpdate() {
    this.saveButtonText = "Loading...";
    this.booking.status = this.status;
    this.apiService.updateBookingById(this.booking).subscribe((res: ResponseFormat) => {
      this.logger.debug("Update Booking: " + JSON.stringify(res));
      this.modalController.dismiss("update");
      if (!res.success) {
        this.synergeService.handleError(res.message);
      }
    }, error => {
      this.synergeService.handleErrorOnAPI(error);
    });
  }

  async onDelete() {
    const alert = await this.alertController.create({
      header: 'Confirm!',
      message: 'Do you want to delete booking?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
        }, {
          text: 'Yes',
          handler: () => {
            this.deleteButtonText = "Loading...";
            this.apiService.deleteBookingById(this.booking._id).subscribe((res: ResponseFormat) => {
              this.logger.debug("Delete Booking: " + JSON.stringify(res));
              this.modalController.dismiss("update");
              if (!res.success) {
                this.synergeService.handleError(res.message);
              }
            }, error => {
              this.synergeService.handleErrorOnAPI(error);
            });
          }
        }
      ]
    });
    await alert.present();
  }

  dismiss() {
    this.modalController.dismiss();
  }

}
