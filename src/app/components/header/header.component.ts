import { Component, Input, OnInit } from '@angular/core';
import { SynergeService } from 'src/app/services/synerge.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  @Input() title: string;
  @Input() showBackButton: boolean;
  @Input() showMenu: boolean;

  constructor(
    public synergeService: SynergeService
  ) { }

  ngOnInit() { }

  goBack() {
    this.synergeService.goBack();
  }

}
