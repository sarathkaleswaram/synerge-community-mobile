import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ResponseFormat } from 'src/app/domains/response-format';
import { ApiService } from 'src/app/services/api.service';
import { LoggerService } from 'src/app/services/logger.service';
import { SynergeService } from 'src/app/services/synerge.service';

@Component({
  selector: 'app-booking-time',
  templateUrl: './booking-time.component.html',
  styleUrls: ['./booking-time.component.scss'],
})
export class BookingTimeComponent implements OnInit {
  @Input() selectedDate: Date;
  @Input() belongs_to;
  @Input() booking_type;
  selectedSlot: string;
  timeSlots = [
    {
      time: "08:00 AM - 09:00 AM",
      selected: false,
      booked: false
    },
    {
      time: "09:00 AM - 10:00 AM",
      selected: false,
      booked: false
    },
    {
      time: "10:00 AM - 11:00 AM",
      selected: false,
      booked: false
    },
    {
      time: "11:00 AM - 12:00 PM",
      selected: false,
      booked: false
    },
    {
      time: "12:00 PM - 01:00 PM",
      selected: false,
      booked: false
    },
    {
      time: "01:00 PM - 02:00 PM",
      selected: false,
      booked: false
    },
    {
      time: "02:00 PM - 03:00 PM",
      selected: false,
      booked: false
    },
    {
      time: "03:00 PM - 04:00 PM",
      selected: false,
      booked: false
    },
    {
      time: "04:00 PM - 05:00 PM",
      selected: false,
      booked: false
    },
    {
      time: "05:00 PM - 06:00 PM",
      selected: false,
      booked: false
    },
    {
      time: "06:00 PM - 07:00 PM",
      selected: false,
      booked: false
    },
    {
      time: "07:00 PM - 08:00 PM",
      selected: false,
      booked: false
    }
  ];

  constructor(
    private apiService: ApiService,
    public synergeService: SynergeService,
    private modalController: ModalController,
    private logger: LoggerService
  ) { }

  ngOnInit() {
    this.selectedDate = new Date(this.selectedDate);
    let date = new Date(this.selectedDate.getFullYear(), this.selectedDate.getMonth(), this.selectedDate.getDate());
    let data = {
      booking_date: date,
      belongs_to: this.belongs_to,
      booking_type: this.booking_type
    };
    this.apiService.getBookingAvailableSlots(data).subscribe((res: ResponseFormat) => {
      this.logger.debug("Booking Available Slots data: " + JSON.stringify(res));
      if (res.success) {
        res.data.forEach(data => {
          let index = this.timeSlots.findIndex(x => x.time === data.booking_time);
          if (index >= 0) {
            this.logger.debug("Some Slots booked for Discussion room");
            this.timeSlots[index].booked = true;
          }
        });
        this.logger.debug("TimeSlots after filter: " + JSON.stringify(this.timeSlots));
      } else {
        this.synergeService.handleError(res.message);
      }
    }, error => {
      this.synergeService.handleErrorOnAPI(error);
    });
  }

  onSelect(slot) {
    this.timeSlots.forEach(slot => {
      slot.selected = false;
    });
    if (!slot.booked) {
      this.selectedSlot = slot.time;
      slot.selected = true;
    }
  }

  onDone() {
    this.modalController.dismiss({ selectedTime: this.selectedSlot });
  }

  dismiss() {
    this.modalController.dismiss();
  }

}
