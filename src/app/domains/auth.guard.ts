import { Injectable } from '@angular/core';
import { Router, CanActivate, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';
import { MenuController } from '@ionic/angular';
import { AppConstants } from './app-constants';

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(
    public router: Router,
    private menu: MenuController
  ) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    if (!localStorage.getItem(AppConstants.TOKEN)) {
      this.router.navigate(['/login'], { replaceUrl: true });
      this.menu.enable(false);
      return false;
    }
    this.menu.enable(true);
    return true;
  }
}