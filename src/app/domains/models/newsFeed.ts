
export interface Likes {
    by_user_id: string;
    by_user_name: string;
    date: Date;
};

export interface Comments {
    comment: string;
    by_user_id: string;
    by_user_name: string;
    date: Date;
    reply?: Comments;
};

export interface NewsFeed {
    _id?: string;
    by_user_id: string;
    by_user_name: string;
    date: Date;
    content: string;
    image_path?: string;
    likes: Likes[],
    comments: Comments[]
};