type BelongsToEnum = 'SYNERGE_I' | 'SYNERGE_II';

export interface ClientVisitors {
    _id?: string;
    visitor_name: string;
    visitor_phone: string;
    visiting_time: Date;
    purpose_of_visit: string;
    whom_to_contact: string;
    whom_to_contact_id: string;
    visiting_company: string;
    company_belongs_to: BelongsToEnum;
};