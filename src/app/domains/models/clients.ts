export type BelongsToEnum = 'SYNERGE_I' | 'SYNERGE_II';
export type ActiveStatusEnum = 'Active' | 'Inactive';

export interface Clients {
    _id?: string;
    company_name: string;
    belongs_to: BelongsToEnum;
    active_status: ActiveStatusEnum;
    joined_date: Date;
    leaving_date?: Date;
};
