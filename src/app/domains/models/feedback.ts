export type FeedbackStatusEnum = 'Not Seen' | 'On Progress' | 'Completed';
export type BelongsToEnum = 'SYNERGE_I' | 'SYNERGE_II';

export interface Comments {
    comment: string;
    by_user_id: string;
    by_user_name: string;
    date: Date;
    reply?: Comments;
};

export interface Feedback {
    _id?: string;
    by_user_id: string;
    by_user_name: string;
    feedback: string;
    requested_date: Date;
    belongs_to: BelongsToEnum;
    company_name: string;
    comments: Comments[];
    status: FeedbackStatusEnum;
};