export type BelongsToEnum = 'SYNERGE_I' | 'SYNERGE_II';

export interface CompanyHead {
  designation: string;
  name: string;
};

export interface SynergeFamily {
  _id?: string;
  belongs_to: BelongsToEnum;
  company_name: string;
  company_description: string;
  company_link: string;
  company_logo_path: string;
  company_heads: CompanyHead[];
};
