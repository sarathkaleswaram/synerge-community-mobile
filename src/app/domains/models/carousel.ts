export interface Carousel {
    _id?: string;
    by_user_id: string;
    by_user_name: string;
    date: Date;
    image_path: string;
};