
export interface Likes {
    by_user_id: string;
    by_user_name: string;
    date: Date;
};

export interface Comments {
    comment: string;
    by_user_id: string;
    by_user_name: string;
    date: Date;
    reply?: Comments;
};

export type EventTypeEnum = 'Upcoming' | 'Completed';
export type BelongsToEnum = 'SYNERGE_I' | 'SYNERGE_II';

export interface Event {    
    _id?: string;
    date: Date;
    belongs_to: BelongsToEnum;
    type: EventTypeEnum;
    title: string;
    description: string;
    pics: string[];
    links: string[];
    likes: Likes[];
    comments: Comments[];
};
