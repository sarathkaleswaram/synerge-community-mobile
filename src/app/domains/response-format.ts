export interface ResponseFormat {
    success: boolean,
    message: string,
    data?: any,
    error?: any,
    token?: string
}

export interface SocketEventFormat {
    type: string,
    message: string,
    url: string
}