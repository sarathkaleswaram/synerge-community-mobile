export const appPages = [
  {
    title: 'Clients',
    url: '/clients',
    icon: 'person'
  },
  {
    title: 'Users',
    url: '/users',
    icon: 'person'
  },
  {
    title: 'Events',
    url: '/events',
    icon: 'game-controller'
  },
  {
    title: 'Feedback/Suggestions',
    url: '/feedback',
    icon: 'reader'
  },
  {
    title: 'Achievers',
    url: '/achievers',
    icon: 'trophy'
  },
  {
    title: 'Synerge Family',
    url: '/synerge-family',
    icon: 'happy'
  }
];