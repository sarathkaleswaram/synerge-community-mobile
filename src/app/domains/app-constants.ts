export class AppConstants {
    public static ROLE_ADMIN = 'ADMIN';
    public static ROLE_CLIENT = 'CLIENT';

    public static TOKEN = 'synerge_token';
    public static ROUTES = 'synerge_routes';
    public static USER_FULL_NAME = 'synerge_full_username';
    public static USER_ID = 'synerge_user_id';
    public static USER_ROLE = 'synerge_user_role';
    public static USER_BELONGS_TO = 'synerge_uesr_belongs_to';
    public static USER_COMPANY_NAME = 'synerge_user_company_name';

    public static ERROR_TOKEN_INVALID = 'Invalid Session or Session expired.';

    public static DARK_EDITION_ACTIVE = 'synerge_dark_edtion_active';

    public static SOCKET_USER = 'socket_user';
    public static SOCKET_EVENT = 'socket_event';

    public static TYPE_LOGOUT = 'App_Logout';
    
    public static TYPE_USER = 'User';
    public static TYPE_BOOKING = 'Booking';
    public static TYPE_CLIENT_VISITOR = 'Client_Visitor';
    public static TYPE_EVENT = 'Event';
    public static TYPE_FEEDBACK = 'Feedback';
    // public static TYPE_ACHIEVER = 'Achiever';
}
