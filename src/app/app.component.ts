import { Component, QueryList, ViewChildren } from '@angular/core';

import { ActionSheetController, IonRouterOutlet, MenuController, ModalController, Platform, PopoverController, ToastController } from '@ionic/angular';
import { SynergeService } from './services/synerge.service';
import { AppConstants } from './domains/app-constants';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  private lastTimeBackPress = 0;
  private timePeriodToExit = 1000;
  loggedIn = false;
  dark = false;
  
  @ViewChildren(IonRouterOutlet) routerOutlets: QueryList<IonRouterOutlet>;

  constructor(
    private platform: Platform,
    public synergeService: SynergeService,
    private menuCtrl: MenuController,
    private modalCtrl: ModalController,
    private actionSheetCtrl: ActionSheetController,
    private popoverCtrl: PopoverController,
    private toastCtrl: ToastController,
    private router: Router
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // this.statusBar.styleDefault();
      // this.splashScreen.hide();
      this.synergeService.setMenuItems();
      this.handleAndroidNativeBackButton();
      let selectedTheme = localStorage.getItem(AppConstants.DARK_EDITION_ACTIVE);
      if (!selectedTheme) {
        localStorage.setItem(AppConstants.DARK_EDITION_ACTIVE, 'true');
        this.dark = true;
      } else {
        if (selectedTheme === 'true') {
          this.dark = true;
        } else {
          this.dark = false;
        }
      }
    });
  }

  handleAndroidNativeBackButton() {
    this.platform.backButton.subscribeWithPriority(999999, async () => {
      // close action sheet
      try {
        const element = await this.actionSheetCtrl.getTop();
        if (element) {
          element.dismiss();
          return;
        }
      } catch (error) {
      }

      // close popover
      try {
        const element = await this.popoverCtrl.getTop();
        if (element) {
          element.dismiss();
          return;
        }
      } catch (error) {
      }

      // close modal
      try {
        const element = await this.modalCtrl.getTop();
        if (element) {
          element.dismiss();
          return;
        }
      } catch (error) {
      }

      // close side menu
      try {
        const element = await this.menuCtrl.getOpen();
        if (element) {
          this.menuCtrl.close();
          return;
        }
      } catch (error) {
      }

      // close toast
      try {
        const element = await this.toastCtrl.getTop();
        if (element) {
          if (element.message !== "Press back again to exit App.") {
            element.dismiss();
            return;
          }
        }
      } catch (error) {
      }

      this.routerOutlets.forEach(async (outlet: IonRouterOutlet) => {
        if (outlet && outlet.canGoBack()) {
          outlet.pop();
        } else if ((new Date().getTime() - this.lastTimeBackPress) < this.timePeriodToExit) {
          navigator['app'].exitApp();
        } else {
          const toast = await this.toastCtrl.create({
            message: 'Press back again to exit App.',
            duration: this.timePeriodToExit
          });
          toast.present();
          this.lastTimeBackPress = new Date().getTime();
        }
      });
    });
  }

  checkLoginStatus() {
    // return this.userData.isLoggedIn().then(loggedIn => {
    //   return this.updateLoggedInStatus(loggedIn);
    // });
  }

  onDarkToggle() {
    if (this.dark) {
      localStorage.setItem(AppConstants.DARK_EDITION_ACTIVE, 'true');
    } else {
      localStorage.setItem(AppConstants.DARK_EDITION_ACTIVE, 'false');
    }
  }

  updateLoggedInStatus(loggedIn: boolean) {
    setTimeout(() => {
      this.loggedIn = loggedIn;
    }, 300);
  }

  listenForLoginEvents() {
    window.addEventListener('user:login', () => {
      this.updateLoggedInStatus(true);
    });

    window.addEventListener('user:signup', () => {
      this.updateLoggedInStatus(true);
    });

    window.addEventListener('user:logout', () => {
      this.updateLoggedInStatus(false);
    });
  }

  onAboutUs() {
    this.router.navigate(['/about-us']);
  }

  logout() {
    this.synergeService.confirmLogout();
  }
}
