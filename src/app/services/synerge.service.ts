import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController, ModalController, NavController, Platform, ToastController } from '@ionic/angular';
import { Clipboard } from '@ionic-native/clipboard/ngx';
import { AppConstants } from 'src/app/domains/app-constants';
import { LoggerService } from './logger.service';
import { basePath } from 'src/app/config';
import { appPages } from '../domains/menu';
import { SocketService } from './socket.service';
import { ApiService } from './api.service';
import { UserInfoComponent } from '../components/user-info/user-info.component';

@Injectable({
  providedIn: 'root'
})
export class SynergeService {
  basePath: string = basePath;
  isMobile = false;
  menuItems = [];
  isAdmin = false;

  constructor(
    private router: Router,
    private platform: Platform,
    private navController: NavController,
    private socketService: SocketService,
    public alertController: AlertController,
    private toastController: ToastController,
    private modalController: ModalController,
    private clipboard: Clipboard,
    public apiService: ApiService,
    private logger: LoggerService
  ) {
    this.isMobile = this.platform.is('cordova');
    this.logger.debug('Platforms: ' + this.platform.platforms());
    this.logger.debug('Platform isMobile: ' + this.isMobile);
    this.isAdmin = localStorage.getItem(AppConstants.USER_ROLE) === 'ADMIN' ? true : false;
  }

  logout() {
    this.logger.debug("Logging Out");
    this.closeModal();
    this.socketService.disconnectSocket();
    localStorage.removeItem(AppConstants.ROLE_ADMIN);
    localStorage.removeItem(AppConstants.TOKEN);
    localStorage.removeItem(AppConstants.ROUTES);
    localStorage.removeItem(AppConstants.USER_FULL_NAME);
    localStorage.removeItem(AppConstants.USER_ID);
    localStorage.removeItem(AppConstants.USER_ROLE);
    localStorage.removeItem(AppConstants.USER_BELONGS_TO);
    localStorage.removeItem(AppConstants.USER_COMPANY_NAME);
    this.router.navigate(['/login'], { replaceUrl: true });
  }

  setMenuItems() {
    this.menuItems = [];
    let routes: [] = JSON.parse(localStorage.getItem(AppConstants.ROUTES)) || [];
    routes.forEach(route => {
      let menu = appPages.find(menu => menu.title == route);
      if (menu) this.menuItems.push(menu);
    });
  }

  onErrorModalClick() {
    this.logout();
  }

  handleErrorOnAPI(error) {
    console.log("Error: ", error);
    if (!error.error) return;
    let doLogout = false;
    let message = error.error ? error.error.message : error.statusText;
    if (error.error.message == AppConstants.ERROR_TOKEN_INVALID) {
      message += ' Logging out.';
      doLogout = true;
    }
    if (!message)
      message = 'Unknow error occurred. Please try again later.';
    this.logger.debug('Error message: ' + message);
    let alert = this.displayAlert(JSON.stringify(message));
    if (doLogout)
      alert.then(a => {
        a.onDidDismiss().then(() => {
          this.logout();
        });
      });
  }

  async displayAlert(message: string, header?: string) {
    this.logger.debug('Alert message: ' + message + ', header: ' + header);
    const alert = await this.alertController.create({
      buttons: ['OK']
    });
    if (header) { alert.header = header; }
    if (message) { alert.message = message; }
    await alert.present();
    return alert;
  }

  handleError(error) {
    this.logger.error('Handle error:' + error);
    if (error.error && error.error.data) {
      this.displayAlert(error.error.data);
    } else if (error.error && error.error.message) {
      this.displayAlert(error.error.message);
      if (error.error.message === 'Invalid Session or Session expired.')
        this.logout();
    } else {
      if (error.message) {
        this.displayAlert(JSON.stringify(error.message));
      } else {
        this.displayAlert(JSON.stringify(error));
      }
    }
  }

  async onUserInfo(user_id: string) {
    const modal = await this.modalController.create({
      component: UserInfoComponent,
      mode: 'ios',
      swipeToClose: true,
      componentProps: { userId: user_id }
    });
    await modal.present();
    // this.router.navigate(['/user-info'], { queryParams: { user_id: user_id } });
  }

  async confirmAlert(message: string, onConfirm: any) {
    const alert = await this.alertController.create({
      header: 'Confirm!',
      message: message,
      buttons: [
        {
          text: 'No',
          role: 'cancel',
        }, {
          text: 'Yes',
          handler: () => {
            onConfirm();
          }
        }
      ]
    });
    await alert.present();
  }

  async confirmLogout() {
    const alert = await this.alertController.create({
      header: 'Confirm!',
      message: 'Do you want to <strong>logout ?</strong>',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
        }, {
          text: 'Okay',
          handler: () => {
            this.logout();
          }
        }
      ]
    });
    await alert.present();
  }

  async displayToast(message, header?, position?, buttons?, duration?) {
    this.logger.debug('Toast message: ' + message);
    const toast = await this.toastController.create({
      message,
      position: position || 'top',
      duration: duration ? duration : 2000
    });
    if (header) { toast.header = header; }
    if (buttons) { toast.buttons = buttons; }
    toast.present();
  }

  closeModal() {
    this.modalController.getTop().then(modal => {
      if (modal)
        this.modalController.dismiss();
    });
  }

  goBack() {
    this.navController.back();
  }

  copyToClipboard(copyText: string) {
    this.clipboard.copy(copyText);
    this.displayToast('Copied!');
  }

  dataURItoBlob(dataURI) {
    let byteString;
    if (dataURI.split(',')[0].indexOf('base64') >= 0)
      byteString = atob(dataURI.split(',')[1]);
    else
      byteString = unescape(dataURI.split(',')[1]);

    let mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

    let ia = new Uint8Array(byteString.length);
    for (let i = 0; i < byteString.length; i++) {
      ia[i] = byteString.charCodeAt(i);
    }
    return new Blob([ia], { type: mimeString });
  }
}
