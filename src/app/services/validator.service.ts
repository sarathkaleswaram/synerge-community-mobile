import { Injectable } from '@angular/core';
import { FormControl, ValidationErrors } from '@angular/forms';
import * as md5 from 'md5';

@Injectable({
  providedIn: 'root'
})
export class ValidatorService {

  constructor() { }

  static phoneMinLength = (control: FormControl): ValidationErrors => {
    if (control.value.toString().length > 0)
      return control.value.toString().length > 7 ? null : { phoneMinLength: true };
  }

  static validateOldPassword = (control): ValidationErrors => {
    if (control._parent) {
      if (control.value.length > 0 && control._parent.value.oldPassword !== md5(control.value)) {
        return { invalidPassword: true }
      } else {
        return null;
      }
    }
    return null;
  }

  static validateNewPassword = (control): ValidationErrors => {
    if (control._parent) {
      if (control.value.length > 0 && control._parent.value.newPassword !== control.value) {
        return { passwordMismatch: true }
      } else {
        return null;
      }
    }
    return null;
  }
}
