import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

import { AppConstants } from '../domains/app-constants';
import { User } from 'src/app/domains/models/user.js';
import { NewsFeed } from 'src/app/domains/models/newsFeed.js';
import { Booking } from 'src/app/domains/models/bookings.js';
import { Clients } from 'src/app/domains/models/clients.js';
import { ClientVisitors } from 'src/app/domains/models/clientVisitors.js';
import { Event } from 'src/app/domains/models/events.js';
import { Feedback } from 'src/app/domains/models/feedback.js';
import { SynergeFamily } from 'src/app/domains/models/synergeFamily.js';
import { Achiever } from 'src/app/domains/models/achievers.js';
import { basePath } from 'src/app/config';
import { Carousel } from '../domains/models/carousel';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  
  private defaultHeaders = new HttpHeaders();
  private basePath = basePath;

  constructor(private http: HttpClient) {
    this.setToken();
  }

  setToken() {
    this.defaultHeaders = this.defaultHeaders.set('x-jwt-auth', localStorage.getItem(AppConstants.TOKEN) || "");
  }

  // FCM subscribe
  fcmSubcribe(body): Observable<any> {
    this.setToken();
    return this.http.post(this.basePath + '/fcm-subscription', body, { headers: this.defaultHeaders });
  }

  forgotPassword(body): Observable<any> {
    return this.http.post(this.basePath + '/forgot-password', body);
  }

  // User
  loginUser(body): Observable<any> {
    return this.http.post(this.basePath + '/login', body);
  }

  registerUser(body): Observable<any> {
    return this.http.post(this.basePath + '/user', body);
  }

  getAllUsers(): Observable<any> {
    this.setToken();
    return this.http.get(this.basePath + '/user', { headers: this.defaultHeaders });
  }

  getUserById(id): Observable<any> {
    this.setToken();
    return this.http.get(this.basePath + '/user/' + id, { headers: this.defaultHeaders });
  }

  updateUser(user: User): Observable<any> {
    this.setToken();
    return this.http.put(this.basePath + '/user/' + user._id, user, { headers: this.defaultHeaders });
  }

  deleteUser(user: User): Observable<any> {
    this.setToken();
    return this.http.delete(this.basePath + '/user/' + user._id, { headers: this.defaultHeaders });
  }


  // Image Upload
  uploadImage(formData): Observable<any> {
    this.setToken();
    return this.http.post(this.basePath + '/upload', formData, { headers: this.defaultHeaders });
  }


  // News Feed
  saveNewsFeed(newsFeed: NewsFeed): Observable<any> {
    this.setToken();
    return this.http.post(this.basePath + '/news-feed', newsFeed, { headers: this.defaultHeaders });
  }

  getAllNewsFeeds(): Observable<any> {
    this.setToken();
    return this.http.get(this.basePath + '/news-feed', { headers: this.defaultHeaders });
  }

  getNewsFeedById(id): Observable<any> {
    this.setToken();
    return this.http.get(this.basePath + '/news-feed/' + id, { headers: this.defaultHeaders });
  }

  updateNewsFeedById(newsFeed: NewsFeed): Observable<any> {
    this.setToken();
    return this.http.put(this.basePath + '/news-feed/' + newsFeed._id, newsFeed, { headers: this.defaultHeaders });
  }

  deleteNewsFeedById(newsFeedId): Observable<any> {
    this.setToken();
    return this.http.delete(this.basePath + '/news-feed/' + newsFeedId, { headers: this.defaultHeaders });
  }

  // Carousel
  saveCarousel(carousel: Carousel): Observable<any> {
    this.setToken();
    return this.http.post(this.basePath + '/carousel', carousel, { headers: this.defaultHeaders });
  }

  getAllCarousels(): Observable<any> {
    this.setToken();
    return this.http.get(this.basePath + '/carousel', { headers: this.defaultHeaders });
  }

  getCarouselById(id): Observable<any> {
    this.setToken();
    return this.http.get(this.basePath + '/carousel/' + id, { headers: this.defaultHeaders });
  }

  updateCarouselById(carousel: Carousel): Observable<any> {
    this.setToken();
    return this.http.put(this.basePath + '/carousel/' + carousel._id, carousel, { headers: this.defaultHeaders });
  }

  deleteCarouselById(carouselId): Observable<any> {
    this.setToken();
    return this.http.delete(this.basePath + '/carousel/' + carouselId, { headers: this.defaultHeaders });
  }

  // Bookings
  addBooking(body): Observable<any> {
    this.setToken();
    return this.http.post(this.basePath + '/booking', body, { headers: this.defaultHeaders });
  }

  getAllBookings(): Observable<any> {
    this.setToken();
    return this.http.get(this.basePath + '/booking', { headers: this.defaultHeaders });
  }

  getBookingById(id): Observable<any> {
    this.setToken();
    return this.http.get(this.basePath + '/booking/' + id, { headers: this.defaultHeaders });
  }

  updateBookingById(booking: Booking): Observable<any> {
    this.setToken();
    return this.http.put(this.basePath + '/booking/' + booking._id, booking, { headers: this.defaultHeaders });
  }

  deleteBookingById(bookingId): Observable<any> {
    this.setToken();
    return this.http.delete(this.basePath + '/booking/' + bookingId, { headers: this.defaultHeaders });
  }

  getBookingAvailableSlots(data): Observable<any> {
    this.setToken();
    return this.http.post(this.basePath + '/bookingAvailableSlots', data, { headers: this.defaultHeaders });
  }


  //Clients
  getAllClients(): Observable<any> {
    this.setToken();
    return this.http.get(this.basePath + '/client', { headers: this.defaultHeaders });
  }

  getAllActiveClients(): Observable<any> {
    this.setToken();
    return this.http.get(this.basePath + '/client/active', { headers: this.defaultHeaders });
  }

  createClient(client: Clients): Observable<any> {
    this.setToken();
    return this.http.post(this.basePath + '/client', client, { headers: this.defaultHeaders });
  }

  getClientById(clientId): Observable<any> {    
    this.setToken();
    return this.http.get(this.basePath + '/client/' + clientId, { headers: this.defaultHeaders });
  }

  updateClientById(client: Clients): Observable<any> {    
    this.setToken();
    return this.http.put(this.basePath + '/client/' + client._id, client, { headers: this.defaultHeaders });
  }

  deleteClientById(client: Clients) {    
    this.setToken();
    return this.http.delete(this.basePath + '/client/' + client._id, { headers: this.defaultHeaders });
  }


  //Client Visitors
  getAllClientVisitors(): Observable<any> {
    this.setToken();
    return this.http.get(this.basePath + '/client-visitors', { headers: this.defaultHeaders });
  }

  createClientVisitor(clientVisitors: ClientVisitors): Observable<any> {
    this.setToken();
    return this.http.post(this.basePath + '/client-visitors', clientVisitors, { headers: this.defaultHeaders });
  }

  deleteClientVisitorById(clientVisitors: ClientVisitors) {    
    this.setToken();
    return this.http.delete(this.basePath + '/client-visitors/' + clientVisitors._id, { headers: this.defaultHeaders });
  }


  // Events
  createEvent(event: Event): Observable<any> {
    this.setToken();
    return this.http.post(this.basePath + '/event', event, { headers: this.defaultHeaders });
  }

  getAllEvents(): Observable<any> {
    this.setToken();
    return this.http.get(this.basePath + '/event', { headers: this.defaultHeaders });
  }

  getEventById(eventId): Observable<any> {
    this.setToken();
    return this.http.get(this.basePath + '/event/' + eventId, { headers: this.defaultHeaders });
  }

  updateEventById(event: Event): Observable<any> {
    this.setToken();
    return this.http.put(this.basePath + '/event/' + event._id, event, { headers: this.defaultHeaders });
  }

  deleteEventById(eventId: string): Observable<any> {
    this.setToken();
    return this.http.delete(this.basePath + '/event/' + eventId, { headers: this.defaultHeaders });
  }


  // Feedback
  createFeedback(feedback: Feedback): Observable<any> {
    this.setToken();
    return this.http.post(this.basePath + '/feedback', feedback, { headers: this.defaultHeaders });
  }

  getAllFeedbacks(): Observable<any> {
    this.setToken();
    return this.http.get(this.basePath + '/feedback', { headers: this.defaultHeaders });
  }

  getFeedbackById(feedbackId): Observable<any> {
    this.setToken();
    return this.http.get(this.basePath + '/feedback/' + feedbackId, { headers: this.defaultHeaders });
  }

  updateFeedbackById(feedback: Feedback): Observable<any> {
    this.setToken();
    return this.http.put(this.basePath + '/feedback/' + feedback._id, feedback, { headers: this.defaultHeaders });
  }

  deleteFeedbackById(feedbackId: string): Observable<any> {
    this.setToken();
    return this.http.delete(this.basePath + '/feedback/' + feedbackId, { headers: this.defaultHeaders });
  }


  // SynergeFamily
  createSynergeFamily(synergeFamily: SynergeFamily): Observable<any> {
    this.setToken();
    return this.http.post(this.basePath + '/synerge-family', synergeFamily, { headers: this.defaultHeaders });
  }

  getAllSynergeFamilys(): Observable<any> {
    this.setToken();
    return this.http.get(this.basePath + '/synerge-family', { headers: this.defaultHeaders });
  }

  getSynergeFamilyById(synergeFamilyId): Observable<any> {
    this.setToken();
    return this.http.get(this.basePath + '/synerge-family/' + synergeFamilyId, { headers: this.defaultHeaders });
  }

  updateSynergeFamilyById(synergeFamily: SynergeFamily): Observable<any> {
    this.setToken();
    return this.http.put(this.basePath + '/synerge-family/' + synergeFamily._id, synergeFamily, { headers: this.defaultHeaders });
  }

  deleteSynergeFamilyById(synergeFamilyId: string): Observable<any> {
    this.setToken();
    return this.http.delete(this.basePath + '/synerge-family/' + synergeFamilyId, { headers: this.defaultHeaders });
  }


  // Achiever
  createAchiever(achiever: Achiever): Observable<any> {
    this.setToken();
    return this.http.post(this.basePath + '/achiever', achiever, { headers: this.defaultHeaders });
  }

  getAllAchievers(): Observable<any> {
    this.setToken();
    return this.http.get(this.basePath + '/achiever', { headers: this.defaultHeaders });
  }

  getAchieverById(achieverId): Observable<any> {
    this.setToken();
    return this.http.get(this.basePath + '/achiever/' + achieverId, { headers: this.defaultHeaders });
  }

  updateAchieverById(achiever: Achiever): Observable<any> {
    this.setToken();
    return this.http.put(this.basePath + '/achiever/' + achiever._id, achiever, { headers: this.defaultHeaders });
  }

  deleteAchieverById(achieverId: string): Observable<any> {
    this.setToken();
    return this.http.delete(this.basePath + '/achiever/' + achieverId, { headers: this.defaultHeaders });
  }

}
