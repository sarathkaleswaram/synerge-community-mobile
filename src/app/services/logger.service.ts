import { Injectable } from '@angular/core';
import { logLevel } from 'src/app/config';

@Injectable({
  providedIn: 'root'
})
export class LoggerService {

  loglevel: string = logLevel;

  constructor() { }

  debug(content: string) {
      if (this.loglevel == 'debug') {
          console.debug(content);
      }
  }

  info(content: string, ...optionalParams: any[]) {
      if (this.loglevel == 'debug' || this.loglevel == 'info') {
          console.info(content, optionalParams);
      }
  }

  error(content: string) {
      if (this.loglevel == 'debug' || this.loglevel == 'info' || this.loglevel == 'error') {
          console.error(content);
      }
  }
}
