import { Injectable } from '@angular/core';
import { basePath } from 'src/app/config';
import * as io from 'socket.io-client';
import { Subject } from 'rxjs';
import { Router } from '@angular/router';
import { LoggerService } from './logger.service';
import { AppConstants } from '../domains/app-constants';
import { SocketEventFormat } from '../domains/response-format';
// import { NotificationService } from './notification.service.js';

@Injectable({
  providedIn: 'root'
})
export class SocketService {
  public sideMenuBlinkItem: string;
  public sideMenuDotItem: string;

  private socket: SocketIOClient.Socket;
  private basePath = basePath;

  private socketSubject = new Subject<SocketEventFormat>();
  socketObservable = this.socketSubject.asObservable();

  constructor(private logger: LoggerService,
    // private notificationService: NotificationService,
    private router: Router) { }

  connectSocket() {
    this.logger.debug("BasePath: " + this.basePath);
    this.logger.debug("Connect Socket");
    this.socket = io(this.basePath, { transports: ['websocket'] });
    this.socket.on('connect', () => {
      this.logger.debug("Connected Socket Id: " + this.socket.id);
      this.socket.emit(AppConstants.SOCKET_USER, {
        socket_id: this.socket.id,
        token: localStorage.getItem(AppConstants.TOKEN),
        full_name: localStorage.getItem(AppConstants.USER_FULL_NAME)
      });
    });
    this.socket.on(AppConstants.SOCKET_EVENT, (data: SocketEventFormat) => {
      this.logger.debug("### Socket Update: " + JSON.stringify(data));
      if (data.type === AppConstants.TYPE_USER) {
        if (localStorage.getItem(AppConstants.USER_ROLE) !== AppConstants.ROLE_ADMIN) {
          this.logout();
        }
      } else {
        this.socketSubject.next(data);
      }
      // this.notificationService.openSnackBar(data.message, data.url);
      this.blinkSideMenu(data.type);
      // let url = this.checkPermissionAndGetURL(data.type, data.data);
      // this.logger.debug("URL: " + url);
      // if (url) {
      //   this.openSnackBar(data.message, url);
      //   this.socketSubject.next(data);
      // }
    });
  }

  disconnectSocket() {
    this.socketSubject.next({ type: AppConstants.TYPE_LOGOUT, message: 'Logout', url: '' });
    this.logger.debug("Disconnect Socket");
    if (this.socket)
      this.socket.disconnect();
  }

  private logout() {
    this.logger.debug("Logging Out");
    this.disconnectSocket();
    this.router.navigate(['/login']);
  }

  private blinkSideMenu(type) {
    // this.sideMenuBlinkItem = 'News Feed';
    switch (type) {
      case AppConstants.TYPE_USER:
        this.sideMenuBlinkItem = 'Users';
        this.sideMenuDotItem = 'Users';
        break;
      case AppConstants.TYPE_BOOKING:
        this.sideMenuBlinkItem = 'Bookings';
        this.sideMenuDotItem = 'Bookings';
        break;
      case AppConstants.TYPE_CLIENT_VISITOR:
        this.sideMenuBlinkItem = 'Visitors';
        this.sideMenuDotItem = 'Visitors';
        break;
      case AppConstants.TYPE_EVENT:
        this.sideMenuBlinkItem = 'Events';
        this.sideMenuDotItem = 'Events';
        break;
      case AppConstants.TYPE_FEEDBACK:
        this.sideMenuBlinkItem = 'Feedback/Suggestions';
        this.sideMenuDotItem = 'Feedback/Suggestions';
        break;

      default:
        break;
    }
    setTimeout(() => {
      this.sideMenuBlinkItem = null;
    }, 2000);
  }

  public resetDotNotification() {
    this.sideMenuDotItem = undefined;
  }

  // checkPermissionAndGetURL(type, data): string {
  //   this.logger.debug("Checking Permission Type: " + type + ", Data: " + JSON.stringify(data));
  //   if (localStorage.getItem(AppConstants.USER_ROLE) === AppConstants.ROLE_ADMIN) {
  //     switch (type) {
  //       case AppConstants.TYPE_USER:
  //         return 'users?userId=' + data._id;
  //       case AppConstants.TYPE_BOOKING:
  //         return 'all-bookings?bookingId=' + data._id;
  //       case AppConstants.TYPE_CLIENT_VISITOR:
  //         return;
  //       case AppConstants.TYPE_EVENT:
  //         return;
  //       case AppConstants.TYPE_FEEDBACK:
  //         return 'feedback?feedbackId=' + data._id;
  //       case AppConstants.TYPE_ACHIEVER:
  //         return;

  //       default:
  //         return;
  //     }
  //   } else {
  //     switch (type) {
  //       case AppConstants.TYPE_USER:
  //         return;
  //       case AppConstants.TYPE_BOOKING:
  //         let booking: Booking = data;
  //         if (booking.by_user_id === localStorage.getItem(AppConstants.USER_ID))
  //           return 'my-booking?bookingId=' + data._id;
  //         break;
  //       case AppConstants.TYPE_CLIENT_VISITOR:
  //         if (!data) return 'my-visitors';
  //         let clientVisitor: ClientVisitors = data;
  //         if (clientVisitor.visiting_company === localStorage.getItem(AppConstants.USER_COMPANY_NAME))
  //           return 'my-visitors?visitorId=' + data._id;
  //         break;
  //       case AppConstants.TYPE_EVENT:
  //         if (!data) return 'events';
  //         let event: Event = data;
  //         if (event.belongs_to === localStorage.getItem(AppConstants.USER_BELONGS_TO))
  //           return 'events?eventId=' + data._id;
  //         break;
  //       case AppConstants.TYPE_FEEDBACK:
  //         if (!data) return 'feedback';
  //         let feedback: Feedback = data;
  //         if (feedback.by_user_id === localStorage.getItem(AppConstants.USER_ID))
  //           return 'feedback?feedbackId=' + data._id;
  //         break;
  //       case AppConstants.TYPE_ACHIEVER:
  //         return;

  //       default:
  //         return;
  //     }
  //     return;
  //   }
  // }

  // openSnackBar(message: string, url: string) {
  //   let snackBarRef = this.snackBar.open(message, url ? 'Open' : '', {
  //     duration: 5000,
  //   });
  //   snackBarRef.onAction().subscribe(() => {
  //     this.logger.debug("Snack-bar Action clicked redirecting: " + url);
  //     this.router.navigateByUrl(url);
  //   });
  // }

  // send(message): void {
  //   this.socket.emit('message', message);
  // }

  // onMessage(): Observable<any> {
  //   return new Observable<any>(observer => {
  //     this.socket.on('message', (data: any) => observer.next(data));
  //   });
  // }

  // onEvent(event): Observable<any> {
  //   return new Observable<any>(observer => {
  //     this.socket.on(event, () => observer.next());
  //   });
  // }
}
