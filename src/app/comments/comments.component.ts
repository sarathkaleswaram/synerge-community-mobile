import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { AppConstants } from '../domains/app-constants';
import { Comments, Event } from '../domains/models/events';
import { Feedback } from '../domains/models/feedback';
import { ResponseFormat } from '../domains/response-format';
import { ApiService } from '../services/api.service';
import { LoggerService } from '../services/logger.service';
import { SynergeService } from '../services/synerge.service';

@Component({
  selector: 'app-comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.scss'],
})
export class CommentsComponent implements OnInit {
  @Input() event: Event;
  @Input() feedback: Feedback;

  comments: Comments[] = [];

  newComment = "";

  constructor(
    public synergeService: SynergeService,
    private apiService: ApiService,
    private logger: LoggerService,
    private modalController: ModalController,
  ) { }

  ngOnInit() {
    this.setComments();
  }

  setComments() {
    if (this.event)
      this.comments = this.event.comments;
    else if (this.feedback)
      this.comments = this.feedback.comments;
  }

  getEvent(event: Event) {
    this.apiService.getEventById(event._id).subscribe((res: ResponseFormat) => {
      if (res.success) {
        this.event = res.data;
        this.setComments();
      } else {
        this.synergeService.handleError(res.message);
      }
    }, error => {
      this.synergeService.handleErrorOnAPI(error);
    });
  }

  getFeedback(feedback: Feedback) {
    this.apiService.getFeedbackById(feedback._id).subscribe((res: ResponseFormat) => {
      if (res.success) {
        this.feedback = res.data;
        this.setComments();
      } else {
        this.synergeService.handleError(res.message);
      }
    }, error => {
      this.synergeService.handleErrorOnAPI(error);
    });
  }

  onSend() {
    let comment: Comments = {
      comment: this.newComment,
      by_user_id: localStorage.getItem(AppConstants.USER_ID),
      by_user_name: localStorage.getItem(AppConstants.USER_FULL_NAME),
      date: new Date()
    };
    this.newComment = "";
    if (this.event) {
      this.event.comments.push(comment);
      this.apiService.updateEventById(this.event).subscribe((res: ResponseFormat) => {
        this.logger.debug("Update events result: " + JSON.stringify(res));
        if (res.success) {
          this.getEvent(this.event);
        } else {
          this.synergeService.handleError(res.message);
        }
      }, error => {
        this.synergeService.handleErrorOnAPI(error);
      });
    }
    else if (this.feedback) {
      this.feedback.comments.push(comment);
      this.apiService.updateFeedbackById(this.feedback).subscribe((res: ResponseFormat) => {
        this.logger.debug("Update feedback result: " + JSON.stringify(res));
        if (res.success) {
          this.getFeedback(this.feedback);
        } else {
          this.synergeService.handleError(res.message);
        }
      }, error => {
        this.synergeService.handleErrorOnAPI(error);
      });
    }
  }

  dismiss() {
    this.modalController.dismiss();
  }

}
